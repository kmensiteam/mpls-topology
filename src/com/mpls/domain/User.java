/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Mensi Kais
 */
@lombok.Data
@Document(collection = "users")
public class User {

    @Id
    private String id;
    private String login;
    private String password;
}
