/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.domain;

import com.mpls.model.LinkModel;
import com.mpls.model.NodeModel;
import java.util.List;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Mensi kais
 */
@Data
@Document(collection = "topologies")
public class Topology {

    @Id
    private String id;
    private String label;
    private List<NodeModel> nodes;
    private List<LinkModel> links;

}
