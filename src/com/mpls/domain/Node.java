/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.domain;

import org.springframework.data.annotation.Id;

/**
 *
 * @author Mensi Kais
 */
@lombok.Data
public class Node {

    @Id
    private String Id;
    private String uid;
    private String type;
    private String label;
    private Point point;
    private String nodeIp;
}
