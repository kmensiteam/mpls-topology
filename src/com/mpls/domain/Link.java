/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 *
 * @author Mensi Kais
 */
@Data
public class Link {

    @Id
    private String id;

    String label;
}
