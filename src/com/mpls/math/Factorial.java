/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.math;

import java.math.BigInteger;
import java.util.Date;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 *
 * @author Mensi kais
 */
public class Factorial {
    static class Pair {
		final BigInteger num;
		final BigInteger value;
		Pair(BigInteger num, BigInteger value) {
			this.num = num;
			this.value = value;
		}
	}
	public static BigInteger factorial(BigInteger num) {
		Stream<Pair> allFactorials = Stream.iterate(new Pair(BigInteger.ONE, BigInteger.ONE),
				x -> new Pair(x.num.add(BigInteger.ONE), x.value.multiply(x.num.add(BigInteger.ONE))));
		// return allFactorials.limit(num.longValue()).reduce((previous,
		// current) -> current).get().value;
		return allFactorials.filter((x) -> x.num.equals(num)).findAny().get().value;
	}
        
        public static void main(String[] args) {
		Date startDate = new Date();
		long startTime = System.nanoTime();
		Supplier<String> supplier1 = () -> "Started at " + startDate;
		System.out.println(supplier1.get());
		
		Supplier<BigInteger> supplier7 = () -> factorial(new BigInteger("100"));
		System.out.println(supplier7.get());
		Date endDate = new Date();
		long totalTime = System.nanoTime() - startTime;
		Supplier<String> supplier2 = () -> "Ended at " + endDate + " total time=" + totalTime + " nanosec";
		System.out.println(supplier2.get());
	}
}
