/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.config;

import com.mpls.model.LinksCollection;
import com.mpls.model.NodesCollection;
import com.mpls.security.view.LoginDialog;
import com.mpls.service.BestEffortProcesingService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 *
 * @author Mensi Kais
 */
@Configuration
@EnableAsync
public class SpringConfig {

    @Bean
    public NodesCollection provideNodesCollection() {
        return new NodesCollection();
    }

    @Bean
    public LinksCollection provideLinksCollection() {
        return new LinksCollection();
    }

    @Bean
    LoginDialog loginDialog() {
        return new LoginDialog();
    }

    @Bean
    BestEffortProcesingService beProcesingService() {
        return new BestEffortProcesingService();
    }
}
