/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.security.view;

import com.mpls.service.UserService;
import de.jensd.fx.glyphs.GlyphIcon;
import de.jensd.fx.glyphs.GlyphsBuilder;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javax.inject.Inject;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author kmensi
 */
public class LoginDialog extends Dialog<String> {

    @Inject
    UserService userService;

    public LoginDialog() {
        super();

        Window window = this.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(e -> {
            System.out.println("dialog closing.........");
            Platform.exit();
        });

    }

    public void init(Stage stage) {
        this.initOwner(stage);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setTitle("Login Dialog!");
        this.setHeaderText("Inupt your password!");

// Set the icon (must be included in the project).
        GlyphIcon icon = GlyphsBuilder.create(FontAwesomeIconView.class).glyph(FontAwesomeIcon.LOCK).size("3em").style("-fx-fill: linear-gradient(#70b4e5 0%, #247cbc 70%, #2c85c1 85%);").build();
        this.setGraphic(icon);

// Set the button types.
        ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().addAll(loginButtonType);

// Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

//        TextField username = new TextField();
//        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");

//        grid.add(new Label("Username:"), 0, 0);
//        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 0);
        grid.add(password, 1, 0);

// Enable/Disable login button depending on whether a username was entered.
        Button loginButton = (Button) this.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        loginButton.addEventFilter(ActionEvent.ACTION, event -> {
            if (!validateAndStore(password)) {
                GridPane g = (GridPane) this.getDialogPane().lookup(".header-panel");
                Label label = (Label) g.lookup(".label");
                g.setStyle("-fx-background-color: #FF6B6B; -fx-font-style: italic;");
                label.setTextFill(Color.WHITE);                
                label.setStyle("-fx-font-size: 18px;");
                label.setText("Wrong Password!");
                event.consume();
            }
        });

// Do some validation (using the Java 8 lambda syntax).
        password.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        this.getDialogPane().setContent(grid);

// Request focus on the username field by default.
        Platform.runLater(() -> password.requestFocus());

// Convert the result to a username-password-pair when the login button is clicked.
//        this.setResultConverter(dialogButton -> {
//            if (dialogButton == loginButtonType) {
//                return password.getText();
//            }
//            return null;
//        });
        this.showAndWait()/*.ifPresent(usernamePassword -> {
            System.out.println("Password=" + password.getText());
            System.out.println("valid password :  " + validateAndStore(password));
        })*/;
    }

    private boolean validateAndStore(PasswordField password) {
        return new BCryptPasswordEncoder().matches(password.getText(), userService.findByLogin("admin").getPassword());
    }

}
