/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.service;

import com.mpls.domain.User;
import com.mpls.repository.UserRepository;
import javax.inject.Inject;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Mensi Kais
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final MongoOperations mongoOps;

    @Inject
    public UserServiceImpl(UserRepository repository, MongoOperations mongoOps) {
        this.repository = repository;
        this.mongoOps = mongoOps;
    }

    @Override
    public User findByLogin(String login) {
        return repository.findByLogin(login);
    }

    @Override
    public User insert(User user) {
        return repository.insert(user);
    }

    @Override
    public User edit(User user) {
        return repository.save(user);
    }

}
