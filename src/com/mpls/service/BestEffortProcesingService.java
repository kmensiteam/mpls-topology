/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.service;

import com.mpls.exception.SubTetaException;
import com.mpls.model.BestEffortPathCollection;
import com.mpls.model.node.BEClass;
import com.mpls.view.NodeLink;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.DoubleStream;
import java.util.stream.LongStream;
import org.springframework.stereotype.Service;

/**
 *
 * @author Mensi Kais
 */
@Service
public class BestEffortProcesingService {

    private Map<BEClass, BestEffortPathCollection> beClassesMap;

    private Map<NodeLink, List<BEClass>> linkByclass = new LinkedHashMap<>();

    public void setBeClassesMap(Map<BEClass, BestEffortPathCollection> beClassesMap) {
        this.beClassesMap = beClassesMap;
    }

    public Map<BEClass, Double> processingBestEffort() {

        beClassesMap.forEach((k, v) -> {
            v.getPathLinksList().forEach(link -> {
                System.out.println(link.getModel().toString());
                linkByclass.put(link, linkByclass.getOrDefault(link, new LinkedList<>()));
                linkByclass.get(link).add(k);
//                if (linkByclass.get(link).contains(k)) {
//                    linkByclass.get(link).add(k);
//                }
            });
        });

        return debitMoyen();
    }

    private Map<BEClass, Double> debitMoyen() {
        Map<BEClass, Double> debitMoyen = new LinkedHashMap<>();

        beClassesMap.forEach((k, path) -> {
            debitMoyen.put(k, k.getLambda() * k.getSigma() / nombreMoyenDesFlux().get(k));
        });

        return debitMoyen;
    }

    private Map<BEClass, Double> nombreMoyenDesFlux() {
        Map<BEClass, Double> nombreMoyen = new LinkedHashMap<>();

        beClassesMap.forEach((k, path) -> {
            double sum = k.getLambda() * k.getSigma() / k.getD();

            for (int i = 0; i < path.getPathLinksList().size(); i++) {
                double cbe = (double) path.getPathLinksList().get(i).getPropertySheetBestEffortLink()
                        .getItemsList()
                        .stream()
                        .filter(item -> item.getName().equals("C(Mbits/s)"))
                        .map(item -> item.getValue())
                        .findFirst()
                        .get();
                sum += processProbabiliteB().get(path.getPathLinksList().get(i)) * (k.getLambda() * k.getSigma() / (cbe - tetaLinks().get(path.getPathLinksList().get(i))));
            }

            nombreMoyen.put(k, sum);
        });

        return nombreMoyen;
    }

    private Map<NodeLink, Double> processProbabiliteB() {
        Map<NodeLink, Double> probabiliteB = new LinkedHashMap<>();

        linkByclass.forEach((link, v) -> {
            double sum = 0;
            double cbe = (double) link.getPropertySheetBestEffortLink()
                    .getItemsList()
                    .stream()
                    .filter(item -> item.getName().equals("C(Mbits/s)"))
                    .map(item -> item.getValue())
                    .findFirst()
                    .get();

            for (int k = 0; k < v.size(); k++) {
                sum += (1 / (cbe - tetaLinks().get(link))) * (v.get(k).getLambda() * v.get(k).getSigma() * processProbabiliteW().get(link));
            }
            sum += processProbabiliteW().get(link);
            probabiliteB.put(link, sum);
        });

        return probabiliteB;
    }

    private Map<NodeLink, Double> processProbabiliteW() {
        Map<NodeLink, Double> probabiliteW = new LinkedHashMap<>();

        linkByclass.forEach((link, v) -> {
            System.out.println("pZero=" + processProbabiliteZero().get(link) + "sommesProduits=" + sommesProduits().get(link));
            probabiliteW.put(link, processProbabiliteZero().get(link) * sommesProduits().get(link));
        });

        return probabiliteW;
    }

    private Map<NodeLink, Double> processProbabiliteZero() {
        Map<NodeLink, Double> probabiliteZero = new LinkedHashMap<>();

        linkByclass.forEach((link, v) -> {
            double cbe = (double) link.getPropertySheetBestEffortLink()
                    .getItemsList()
                    .stream()
                    .filter(item -> item.getName().equals("C(Mbits/s)"))
                    .map(item -> item.getValue())
                    .findFirst()
                    .get();
            double sub = cbe - tetaLinks().get(link);
            System.out.println("pZero- sub: " + sub);
            if (sub <= 0) {
                //return;
                throw new SubTetaException("C(Mbits/s) is lesser than Teta!");
            } else {
                probabiliteZero.put(link, 1 / (sommesProduits().get(link) + (1 / (cbe - tetaLinks().get(link))) * (tetaLinks().get(link) * sommesProduits().get(link))));
            }
        });

        return probabiliteZero;
    }

    private Map<NodeLink, Double> tetaLinks() {

        Map<NodeLink, Double> tetalinks = new LinkedHashMap<>();

        linkByclass.forEach((key, value) -> {
            tetalinks.put(key, value.stream().flatMapToDouble(cl -> DoubleStream.of(cl.getLambda() * cl.getSigma())).sum());
        });

        tetalinks.forEach((k, v) -> {
            System.out.println("teta- link: " + k.getId() + " == " + v);
        });

        return tetalinks;
    }

    private Map<NodeLink, Double> sommesProduits() {

        Map<NodeLink, Double> sommesProduits = new LinkedHashMap<>();

        linkByclass.forEach((k, v) -> {
            double vsum = 0;
            double cbe = (double) k.getPropertySheetBestEffortLink()
                    .getItemsList()
                    .stream()
                    .filter(item -> item.getName().equals("C(Mbits/s)"))
                    .map(item -> item.getValue())
                    .findFirst()
                    .get();
            for (int m = 0; m < v.size(); m++) {
                double sum = 0;
                for (int n = 0; n <= cbe / v.get(m).getD(); n++) {
                    double prod = 1;
                    for (int i = 0; i < v.size(); i++) {
//                        prod *= Math.pow(v.get(i).getLambda() * v.get(i).getSigma() / v.get(i).getD(), n) / Factorial.factorial(new BigInteger(String.valueOf(n))).doubleValue();
                        prod *= Math.pow(v.get(i).getLambda() * v.get(i).getSigma() / v.get(i).getD(), n) / factorial(n);
                    }
                    sum += prod;
                }
                vsum += sum;
            }
            sommesProduits.put(k, vsum);
        });

        sommesProduits.forEach((k, v) -> {
            System.out.println("sommesProduits- link: " + k.getId() + " == " + v);
        });

        return sommesProduits;
    }

    public long factorial(int n) {
//        if (n > 20) {
//            throw new IllegalArgumentException(n + " is out of range");
//        }
        return LongStream.rangeClosed(1, n).reduce(1, (a, b) -> a * b);
    }
}
