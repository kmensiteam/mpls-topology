/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.service;

import com.mpls.domain.Topology;
import com.mpls.repository.TopologyRepository;
import java.util.List;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Mensi Kais
 */
@Service
@Transactional
public class TopologyServiceImpl implements TopologyService {

    private final TopologyRepository repository;
    private final MongoOperations mongoOps;

    @Inject
    public TopologyServiceImpl(TopologyRepository repository, MongoOperations mongoOps) {
        this.repository = repository;
        this.mongoOps = mongoOps;
    }

    @Override
    public List<Topology> findAll() {
        return repository.findAll();
    }

    @Override
    public Topology findOne(String id) {
        return repository.findOne(new ObjectId(id));
    }

    @Override
    public Topology findByLabel(String label) {
        return repository.findByLabel(label);
    }

    @Override
    public Topology insert(Topology topology) {
        return repository.insert(topology);
    }

    @Override
    public Topology save(Topology topology) {
        return repository.save(topology);
    }

    @Override
    public Topology remove(Topology topology) {
        if (topology != null) {
            repository.delete(topology);
        }
        return topology;
    }

    @Override
    public Topology remove(String id) {
        Topology topology = findOne(id);
        if (topology != null) {
            repository.delete(new ObjectId(id));
        }
        return topology;
    }

    @Override
    public void removeAll() {
        repository.deleteAll();
    }

}
