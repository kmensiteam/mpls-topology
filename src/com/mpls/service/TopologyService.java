/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.service;

import com.mpls.domain.Topology;
import java.util.List;

/**
 *
 * @author Mensi Kais
 */
public interface TopologyService {

    public List<Topology> findAll();

    public Topology findOne(String id);

    public Topology findByLabel(String label);

    public Topology insert(Topology topology);

    public Topology save(Topology topology);

    public Topology remove(Topology topology);
    
    public Topology remove(String id);

    public void removeAll();
}
