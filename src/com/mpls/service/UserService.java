/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.service;

import com.mpls.domain.User;

/**
 *
 * @author Mensi Kais
 */
public interface UserService {

    public User findByLogin(String login);

    public User insert(User user);

    public User edit(User user);
}
