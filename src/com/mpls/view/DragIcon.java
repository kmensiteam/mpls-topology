package com.mpls.view;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author kmens
 */
public class DragIcon extends AnchorPane {

    private FXMLLoader loader;

    private String label;
    private DragIconType type;

    public DragIcon() {

        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/drag-icon.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void initialize() {
        getStyleClass().add("dragicon");
        getStyleClass().add("icon-color");
        this.setOpacity(0.9);
    }

    public void relocateToPoint(Point2D p) {

        Point2D localCoords = getParent().sceneToLocal(p);

        relocate(
                (int) (localCoords.getX()
                - (getBoundsInLocal().getWidth() / 2)),
                (int) (localCoords.getY()
                - (getBoundsInLocal().getHeight() / 2))
        );
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public DragIconType getType() {
        return type;
    }

    public void setType(DragIconType type) {
        this.type = type;

        getStyleClass().clear();
        getStyleClass().add("dragicon");
        getStyleClass().add("icon-color");

        switch (this.type) {

            case LSR:
                getStyleClass().add("icon-router-lsr");
                break;

            case LER:
                getStyleClass().add("icon-router-ler");
                break;

            default:
                break;
        }
    }

}
