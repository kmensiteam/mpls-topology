/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view.link;

import com.mpls.model.link.LinkBEDataMap;
import com.mpls.model.link.LinkBEPropertyItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.control.PropertySheet;

/**
 *
 * @author Mensi Kais
 */
public class PropertySheetBestEffortLink extends VBox {

    private final PropertySheet propertySheet;
    ObservableList<PropertySheet.Item> itemsList;

    public PropertySheetBestEffortLink(LinkBEDataMap linkBEDataMap) {
        itemsList = FXCollections.observableArrayList();
        linkBEDataMap.getNodesMap().keySet().forEach((key) -> {
            LinkBEPropertyItem linkBEPropertyItem = new LinkBEPropertyItem();
            linkBEPropertyItem.setElasticData(linkBEDataMap);
            linkBEPropertyItem.setKey(key);
            itemsList.add(linkBEPropertyItem);
        });

        propertySheet = new PropertySheet(itemsList);
        VBox.setVgrow(propertySheet, Priority.ALWAYS);
        getChildren().add(propertySheet);
    }

    public ObservableList<PropertySheet.Item> getItemsList() {
        return itemsList;
    }

}
