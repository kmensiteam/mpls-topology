/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view.link;

import com.mpls.model.link.LinkHDataMap;
import com.mpls.model.link.LinkHPropertyItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.control.PropertySheet;

/**
 *
 * @author Mensi Kais
 */
public class PropertySheetHeterogeneousLink extends VBox {

    private final PropertySheet propertySheet;
    ObservableList<PropertySheet.Item> itemsList;

    public PropertySheetHeterogeneousLink(LinkHDataMap linkHDataMap) {
        itemsList = FXCollections.observableArrayList();
        linkHDataMap.getNodesMap().keySet().forEach((key) -> {
            LinkHPropertyItem linkHPropertyItem = new LinkHPropertyItem();
            linkHPropertyItem.setElasticData(linkHDataMap);
            linkHPropertyItem.setKey(key);
            itemsList.add(linkHPropertyItem);
        });

        propertySheet = new PropertySheet(itemsList);
        VBox.setVgrow(propertySheet, Priority.ALWAYS);
        getChildren().add(propertySheet);
    }

    public ObservableList<PropertySheet.Item> getItemsList() {
        return itemsList;
    }

}
