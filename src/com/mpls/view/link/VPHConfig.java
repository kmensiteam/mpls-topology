/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view.link;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author Mensi Kais
 */
public class VPHConfig extends BorderPane implements Initializable {

    private FXMLLoader loader;

    @FXML
    private ListView<Double> listVPH;
    @FXML
    private TextField inputVPH;

    private List<Double> vphDoubleList = new ArrayList<>();

    public VPHConfig() {
        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/vph-config.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void addVPH(ActionEvent event) {
        listVPH.getItems().add(Double.valueOf(inputVPH.getText()));
    }

    @FXML
    private void deleteVPH(ActionEvent event) {
        listVPH.getItems().remove(listVPH.getSelectionModel().getSelectedItem());
    }

    public List<Double> getVphDoubleList() {
        listVPH.getItems().forEach(i -> {
            vphDoubleList.add(i);
        });
        return vphDoubleList;
    }

    public void setVphDoubleList(List<Double> vphDoubleList) {
        this.vphDoubleList = vphDoubleList;
        listVPH.getItems().addAll(vphDoubleList);
    }

}
