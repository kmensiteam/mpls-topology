/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view.link;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author Mensi Kais
 */
public class VPDSConfig extends BorderPane implements Initializable {

    private FXMLLoader loader;

    @FXML
    private ListView<Double> listVPDS;
    @FXML
    private TextField inputVPDS;

    private List<Double> vpdsDoubleList = new ArrayList<>();

    public VPDSConfig() {
        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/vpds-config.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void addVPDS(ActionEvent event) {
        listVPDS.getItems().add(Double.valueOf(inputVPDS.getText()));
    }

    @FXML
    private void deleteVPDS(ActionEvent event) {
        listVPDS.getItems().remove(listVPDS.getSelectionModel().getSelectedItem());
    }

    public List<Double> getVpdsDoubleList() {
        listVPDS.getItems().forEach(i -> {
            vpdsDoubleList.add(i);
        });
        return vpdsDoubleList;
    }

    public void setVpdsDoubleList(List<Double> vpdsDoubleList) {
        this.vpdsDoubleList = vpdsDoubleList;
        listVPDS.getItems().addAll(vpdsDoubleList);
    }

}
