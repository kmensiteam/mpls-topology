/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.model.node.StreamingPropertyItem;
import com.mpls.model.node.StreamingDataMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.control.PropertySheet;

/**
 *
 * @author Mensi Kais
 */
public class PropertySheetStreamingNode extends VBox {

    private final PropertySheet propertySheet;
    ObservableList<PropertySheet.Item> itemsList;

    private final StreamingDataMap nodesDataMap;

    public PropertySheetStreamingNode(StreamingDataMap nodesData) {
        this.nodesDataMap = nodesData;
        itemsList = FXCollections.observableArrayList();
        nodesData.getNodesMap().keySet().forEach((key) -> {
            StreamingPropertyItem nodePropertyItem = new StreamingPropertyItem();
            nodePropertyItem.setNodesData(nodesData);
            nodePropertyItem.setKey(key);
            itemsList.add(nodePropertyItem);
        });

        propertySheet = new PropertySheet(itemsList);
        VBox.setVgrow(propertySheet, Priority.ALWAYS);
        getChildren().add(propertySheet);
    }

    public ObservableList<PropertySheet.Item> getItemsList() {
        return itemsList;
    }

    public StreamingDataMap getNodesDataMap() {
        return nodesDataMap;
    }

}
