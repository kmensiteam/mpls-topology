/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.view.node.BEClassConfig;
import com.mpls.view.node.DSClassConfig;
import com.mpls.view.node.SClassConfig;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author kmens
 */
public class TrafficConfig extends AnchorPane implements Initializable {

    private FXMLLoader loader;
    
    private BEClassConfig beClassConfig;
    private DSClassConfig dsClassConfig;
    private SClassConfig sClassConfig;

    @FXML
    private BorderPane bestEffortPane;

    @FXML
    private BorderPane divServicePane;
    
    @FXML
    private BorderPane streamingPane;

    public TrafficConfig(
            BEClassConfig beClassConfig,
            DSClassConfig dsClassConfig,
            SClassConfig sClassConfig
    ) {
        this.beClassConfig = beClassConfig;
        this.dsClassConfig = dsClassConfig;
        this.sClassConfig = sClassConfig;
        
        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/traffic-config.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bestEffortPane.setCenter(beClassConfig);
        divServicePane.setCenter(dsClassConfig);
        streamingPane.setCenter(sClassConfig);
    }

}
