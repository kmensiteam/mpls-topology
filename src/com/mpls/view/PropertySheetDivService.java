/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.model.node.DivDataMap;
import com.mpls.model.node.DivPropertyItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.control.PropertySheet;

/**
 *
 * @author Mensi Kais
 */
public class PropertySheetDivService extends VBox {

    private final PropertySheet propertySheet;
    ObservableList<PropertySheet.Item> itemsList;

    public PropertySheetDivService(DivDataMap divData) {
        itemsList = FXCollections.observableArrayList();
        divData.getNodesMap().keySet().forEach((key) -> {
            DivPropertyItem elasticPropertyItem = new DivPropertyItem();
            elasticPropertyItem.setElasticData(divData);
            elasticPropertyItem.setKey(key);
            itemsList.add(elasticPropertyItem);
        });

        propertySheet = new PropertySheet(itemsList);
        VBox.setVgrow(propertySheet, Priority.ALWAYS);
        getChildren().add(propertySheet);
    }

    public ObservableList<PropertySheet.Item> getItemsList() {
        return itemsList;
    }

}
