/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.model.LinkModel;
import com.mpls.model.link.LinkBEDataMap;
import com.mpls.model.link.LinkDSDataMap;
import com.mpls.model.link.LinkHDataMap;
import com.mpls.view.link.PropertySheetBestEffortLink;
import com.mpls.view.link.PropertySheetDivServiceLink;
import com.mpls.view.link.PropertySheetHeterogeneousLink;
import com.mpls.view.link.VPDSConfig;
import com.mpls.view.link.VPHConfig;
import java.util.UUID;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.When;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.StrokeLineCap;
import org.controlsfx.control.PopOver;

/**
 * FXML Controller class
 *
 * @author kmensi
 */
public class NodeLink extends CubicCurve {
    
    public static final String BLACK = "BLACK";
    public static final String BLEU = "BLEU";
    public static final String RED = "RED";
    public static final String GREEN = "GREEN";
    
    private String uid = UUID.randomUUID().toString();
    private String type;
    private String currentType = NodeLink.BLACK;
    private PopOver popOver;
    LinkConfig linkConfig;
    private LinkModel model = new LinkModel();
    LinkBEDataMap linkBEDataMap;
    PropertySheetBestEffortLink propertySheetBestEffortLink;
    LinkDSDataMap linkDSDataMap;
    PropertySheetDivServiceLink propertySheetDivServiceLink;
    LinkHDataMap linkHDataMap;
    PropertySheetHeterogeneousLink propertySheetHeterogeneousLink;
    
    private VPDSConfig vPSDConfig;
    private VPHConfig vPHConfig;
    
    private final DoubleProperty controlOffsetX = new SimpleDoubleProperty();
    private final DoubleProperty controlOffsetY = new SimpleDoubleProperty();
    private final DoubleProperty controlDirectionX1 = new SimpleDoubleProperty();
    private final DoubleProperty controlDirectionY1 = new SimpleDoubleProperty();
    private final DoubleProperty controlDirectionX2 = new SimpleDoubleProperty();
    private final DoubleProperty controlDirectionY2 = new SimpleDoubleProperty();
    
    public NodeLink() {
        controlOffsetX.set(100.0);
        controlOffsetY.set(50.0);
        
        controlDirectionX1.bind(new When(
                this.startXProperty().greaterThan(this.endXProperty()))
                .then(-1.0).otherwise(1.0));
        
        controlDirectionX2.bind(new When(
                this.startXProperty().greaterThan(this.endXProperty()))
                .then(1.0).otherwise(-1.0));
        
        this.controlX1Property().bind(
                Bindings.add(
                        this.startXProperty(), controlOffsetX.multiply(controlDirectionX1)
                )
        );
        
        this.controlX2Property().bind(
                Bindings.add(
                        this.endXProperty(), controlOffsetX.multiply(controlDirectionX2)
                )
        );
        
        this.controlY1Property().bind(
                Bindings.add(
                        this.startYProperty(), controlOffsetY.multiply(controlDirectionY1)
                )
        );
        
        this.controlY2Property().bind(
                Bindings.add(
                        this.endYProperty(), controlOffsetY.multiply(controlDirectionY2)
                )
        );

//        this.setStroke(Color.FORESTGREEN);
        this.setStrokeWidth(4);
        this.setStrokeLineCap(StrokeLineCap.ROUND);
        this.setFill(Color.CORNSILK.deriveColor(0, 1.2, 1, 0.6));
        this.setCursor(Cursor.CROSSHAIR);

        //provide a universally unique identifier for this object
        setId(uid);
        model.setUid(uid);
        setType(NodeLink.BLACK);
        popOver = new PopOver();        
        linkBEDataMap = new LinkBEDataMap(model);
        linkDSDataMap = new LinkDSDataMap(model);
        linkHDataMap = new LinkHDataMap(model);
        
        propertySheetBestEffortLink = new PropertySheetBestEffortLink(linkBEDataMap);
        propertySheetDivServiceLink = new PropertySheetDivServiceLink(linkDSDataMap);
        propertySheetHeterogeneousLink = new PropertySheetHeterogeneousLink(linkHDataMap);
        
        vPSDConfig = new VPDSConfig();
        vPHConfig = new VPHConfig();
        
        linkConfig = new LinkConfig(
                propertySheetBestEffortLink,
                propertySheetDivServiceLink,
                propertySheetHeterogeneousLink,
                vPSDConfig,
                vPHConfig,
                popOver
        );
        
        this.setOnMouseClicked(event -> {
            currentType = this.getType();
            this.setType(NodeLink.RED);
            popOver.setDetached(true);
            popOver.setTitle("Info");
            popOver.setContentNode(linkConfig);
            popOver.show(this, event.getScreenX(), event.getScreenY());
        });
        
        popOver.setOnHiding(event -> {
            this.setType(currentType);
        });
        
    }
    
    public PropertySheetBestEffortLink getPropertySheetBestEffortLink() {
        return propertySheetBestEffortLink;
    }

    public PropertySheetDivServiceLink getPropertySheetDivServiceLink() {
        return propertySheetDivServiceLink;
    }

    public PropertySheetHeterogeneousLink getPropertySheetHeterogeneousLink() {
        return propertySheetHeterogeneousLink;
    }

    public VPDSConfig getvPSDConfig() {
        return vPSDConfig;
    }

    public VPHConfig getvPHConfig() {
        return vPHConfig;
    }    
    
    public CubicCurve getNodeLink() {
        return this;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
        
        this.getStyleClass().clear();
        
        switch (this.type) {
            case "BLACK":
                //this.getStyleClass().add("link-black");
                this.setStyle("-fx-stroke : #000000;");
                break;
            case "BLEU":
                this.getStyleClass().add("link-bleu");
                break;
            case "RED":
                this.getStyleClass().add("link-red");
                break;
            case "GREEN":
                this.getStyleClass().add("link-green");
                break;
        }
        
    }
    
    public LinkModel getModel() {
        return model;
    }
    
    public void setModel(LinkModel model) {
        this.model = model;
    }
    
    public void setStart(Point2D startPoint) {
        
        this.setStartX(startPoint.getX());
        this.setStartY(startPoint.getY());
    }
    
    public void setEnd(Point2D endPoint) {
        
        this.setEndX(endPoint.getX());
        this.setEndY(endPoint.getY());
    }

//    public void bindEnds(DraggableNode source, DraggableNode target) {
//        this.startXProperty().bind(
//                Bindings.add(source.layoutXProperty(), (source.getWidth() / 2.0)));
//
//        this.startYProperty().bind(
//                Bindings.add(source.layoutYProperty(), (source.getWidth() / 2.0)));
//
//        this.endXProperty().bind(
//                Bindings.add(target.layoutXProperty(), (target.getWidth() / 2.0)));
//
//        this.endYProperty().bind(
//                Bindings.add(target.layoutYProperty(), (target.getWidth() / 2.0)));
//
//        source.registerLink(getId());
//        target.registerLink(getId());
//    }
    public void bindEnds(DraggableNode source, DraggableNode target) {
        //TODO
//        System.out.println("source layout: " + source.layoutXProperty() + " width source :" + (source.getWidth() / 2.0));

        this.startXProperty().bind(
                Bindings.add(source.layoutXProperty(), 40));
        
        this.startYProperty().bind(
                Bindings.add(source.layoutYProperty(), 40));
        
        this.endXProperty().bind(
                Bindings.add(target.layoutXProperty(), 40));
        
        this.endYProperty().bind(
                Bindings.add(target.layoutYProperty(), 40));
        
        source.registerLink(getId());
        target.registerLink(getId());
    }
}
