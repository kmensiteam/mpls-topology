package com.mpls.view;

import com.mpls.domain.Point;
import com.mpls.domain.Topology;
import com.mpls.domain.User;
import com.mpls.exception.SubTetaException;
import com.mpls.model.BestEffortPathCollection;
import com.mpls.model.LinkModel;
import com.mpls.model.LinksCollection;
import com.mpls.model.NodeModel;
import com.mpls.model.NodesCollection;
import com.mpls.model.node.BEClass;
import com.mpls.report.BestEffortReportModel;
import com.mpls.report.PdfReport;
import com.mpls.report.view.PdfViewer;
import com.mpls.service.BestEffortProcesingService;
import com.mpls.service.TopologyService;
import com.mpls.service.UserService;
import de.jensd.fx.glyphs.GlyphIcon;
import de.jensd.fx.glyphs.GlyphsBuilder;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.octicons.OctIcon;
import de.jensd.fx.glyphs.octicons.OctIconView;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.Window;
import javax.inject.Inject;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class ViewController implements Initializable {
    
    @Inject
    NodesCollection nodes;
    
    @Inject
    LinksCollection links;
    
    @Inject
    BestEffortProcesingService bestEffortProcesingService;

//    @Inject
//    private MongoOperations mongoOperations;
    @Inject
    TopologyService topologyService;
    
    @Inject
    UserService userService;
    
    private BestEffortPathCollection beCollection = new BestEffortPathCollection();
    private BestEffortReportModel reportModel = new BestEffortReportModel();
    private StringBuilder sb;
    
    @FXML
    private BorderPane borderPane;
    
    private final TextArea console = new TextArea();
    private AnchorPane rightPane;
    @FXML
    private Menu fileMenu;
    @FXML
    private MenuItem btnClose;
    @FXML
    private MenuItem btnOpenDatabase;
    @FXML
    private MenuItem btnAbout;
    @FXML
    private Button btnRun;
    @FXML
    private Button btnClear;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        console.setEditable(false);
//        console.setMaxHeight(100);
        console.setWrapText(true);
        console.setStyle("-fx-margin-left: 10%;");
        
        RootLayout root = new RootLayout();
        borderPane.setCenter(root);
        borderPane.setBottom(console);
        
        rightPane = root.getRightPane();

//        Topology topo = new Topology();
//        topo.setLabel("topo1");
//        topologyService.insert(topo);
//
//        topo = new Topology();
//        topo.setLabel("topo2");
//        topologyService.insert(topo);
//
//        topologyService.findAll().forEach(System.out::println);
//        topologyService.removeAll();
//        mongoOperations.save(topo);
//        
//        Query searchUserQuery = new Query(Criteria.where("label").is("topo1"));        
//        Topology topo1 = mongoOperations.findOne(searchUserQuery, Topology.class);
//        
//        System.out.println("topology: " + topo1.toString());
//        User user = new User();
//        user.setLogin("admin");
//        user.setPassword(new BCryptPasswordEncoder().encode("pass"));
//
//        userService.insert(user);
    }
    
    private void consoleAppend(String text) {
        console.appendText("\t\t\t" + text + "\n");
    }
    
    private void consoleAppend(Object text) {
        console.appendText("\t\t\t" + text + "\n");
    }
    
    @FXML
    private void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
//            FontAwesomeIconView iconf = new FontAwesomeIconView(FontAwesomeIcon.REMOVE);
//            OctIconView icon = new OctIconView(OctIcon.INFO);
        GlyphIcon icon = GlyphsBuilder.create(OctIconView.class).glyph(OctIcon.INFO).size("3em").style("-fx-fill: linear-gradient(#70b4e5 0%, #247cbc 70%, #2c85c1 85%);").build();
        alert.setTitle("About");
        alert.setGraphic(icon);
        alert.setHeaderText("About MPLS Application");
        alert.setContentText("About MPLS Simulation");
        
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/info.png")));
        
        alert.showAndWait();
    }
    
    @FXML
    private void openDatabase(ActionEvent event) {
        Dialog dialog = new Dialog();
//            FontAwesomeIconView iconf = new FontAwesomeIconView(FontAwesomeIcon.FOLDER_OPEN_ALT);
//            OctIconView icon = new OctIconView(OctIcon.INFO);
//        GlyphIcon icon = GlyphsBuilder.create(FontAwesomeIconView.class).glyph(FontAwesomeIcon.FOLDER_OPEN_ALT).size("3em").style("-fx-fill: linear-gradient(#70b4e5 0%, #247cbc 70%, #2c85c1 85%);").build();
        dialog.setTitle("Load DataBase");
        dialog.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/com/mpls/images/load-database.png"))));
        dialog.setHeaderText("Load Topology from Database");
//        dialog.setContentText("Load Topology from database!");

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/load-database.png")));

        // Set the button types.
        ButtonType loadButtonType = new ButtonType("Load", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loadButtonType, ButtonType.CANCEL);

        // Enable/Disable login button depending on whether a username was entered.
        Node loadButton = dialog.getDialogPane().lookupButton(loadButtonType);
        loadButton.setDisable(true);
        
        ListView listView = new ListView();
        
        topologyService.findAll().forEach(topo -> {
            listView.getItems().add(topo.getLabel());
        });

//            GridPane.setVgrow(listView, Priority.ALWAYS);
//            GridPane.setHgrow(listView, Priority.ALWAYS);
        GridPane listContent = new GridPane();
        listContent.setMaxWidth(Double.MAX_VALUE);
        listContent.add(listView, 0, 0);

        // Do some validation (using the Java 8 lambda syntax).
        listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//                System.out.println("ListView selection changed from oldValue = "
//                        + oldValue + " to newValue = " + newValue);
            loadButton.setDisable(false);
        });

        // Set expandable Exception into the dialog pane.
        dialog.getDialogPane().setContent(listContent);
        
        dialog.showAndWait()
                .filter(response -> response == loadButtonType)
                .ifPresent(response -> {
                    consoleAppend("loding topo: " + listView.getSelectionModel().getSelectedItem() + " from database!");
                    
                    Topology topo = topologyService.findByLabel((String) listView.getSelectionModel().getSelectedItem());
                    
                    if (topo != null) {
                        clear(event);

                        // Populate the data nodes from Nodes Model
                        // Create the nodes in Right Pane
                        topo.getNodes().forEach(nodeModel -> {
                            DraggableNode node = new DraggableNode();
                            node.setId(nodeModel.getUid());
                            node.setModel(nodeModel);
                            node.setType(DragIconType.valueOf(nodeModel.getType()));
                            
                            node.getPropertySheetNode()
                                    .getItemsList()
                                    .forEach(item -> {
                                        if (item.getName().equals("Router Id")) {
                                            item.setValue(nodeModel.getId());
                                        }
                                        if (item.getName().equals("Router Name")) {
                                            item.setValue(nodeModel.getName());
                                        }
                                        if (item.getName().equals("Router IP")) {
                                            item.setValue(nodeModel.getIp());
                                        }
                                    });

//                            node.getBeClassConfig().getBeClassList().clear();
                            node.getBeClassConfig().setBeClassList(
                                    nodeModel.getBeClasses()
                            );
//                            node.getDsClassConfig().getDsClassList().clear();
                            node.getDsClassConfig().setDsClassList(
                                    nodeModel.getDsClasses()
                            );
//                            node.getSClassConfig().getSClassList().clear();
                            node.getSClassConfig().setSClassList(
                                    nodeModel.getSClasses()
                            );

//                            node.getPropertySheetBestEffort()
//                                    .getItemsList()
//                                    .forEach(item -> {
//                                        if (item.getName().equals("di(Mbits/s)")) {
//                                            item.setValue(nodeModel.getBeDi());
//                                        }
//                                        if (item.getName().equals("ʎi(flows/s)")) {
//                                            item.setValue(nodeModel.getBeLambdai());
//                                        }
//                                        if (item.getName().equals("σi(Mbits/s)")) {
//                                            item.setValue(nodeModel.getBeSigmai());
//                                        }
//                                    });
//
//                            node.getPropertySheetDivService()
//                                    .getItemsList()
//                                    .forEach(item -> {
//                                        if (item.getName().equals("di(Mbits/s)")) {
//                                            item.setValue(nodeModel.getDivDi());
//                                        }
//                                        if (item.getName().equals("ʎi(flows/s)")) {
//                                            item.setValue(nodeModel.getDivLambdai());
//                                        }
//                                        if (item.getName().equals("σi(Mbits/s)")) {
//                                            item.setValue(nodeModel.getDivSigmai());
//                                        }
//                                    });
//
//                            node.getPropertySheetStreamingNode()
//                                    .getItemsList()
//                                    .forEach(item -> {
//                                        if (item.getName().equals("dj(Mbits/s)")) {
//                                            item.setValue(nodeModel.getDivDi());
//                                        }
//                                        if (item.getName().equals("ʎj(flows/s)")) {
//                                            item.setValue(nodeModel.getDivLambdai());
//                                        }
//                                        if (item.getName().equals("τi(s)")) {
//                                            item.setValue(nodeModel.getDivSigmai());
//                                        }
//                                    });
                            rightPane.getChildren().add(node);
                            
                            node.relocate(nodeModel.getPoint().getX(), nodeModel.getPoint().getY());
                        });

                        // Populate the data links from Links Model
                        // Create the links in Right Pane
                        topo.getLinks().forEach(linkModel -> {
                            NodeLink link = new NodeLink();
                            link.setId(linkModel.getUid());
                            link.setModel(linkModel);
                            link.setType(linkModel.getType());
                            
                            link.getPropertySheetBestEffortLink()
                                    .getItemsList()
                                    .forEach(item -> {
                                        if (item.getName().equals("C(Mbits/s)")) {
                                            item.setValue(linkModel.getCBE());
                                        }
                                    });
                            
                            link.getPropertySheetDivServiceLink()
                                    .getItemsList()
                                    .forEach(item -> {
                                        if (item.getName().equals("C(Mbits/s)")) {
                                            item.setValue(linkModel.getCDS());
                                        }
                                        if (item.getName().equals("WFQ Number")) {
                                            item.setValue(linkModel.getNvpDS());
                                        }
                                    });
                            
                            link.getvPSDConfig().setVpdsDoubleList(linkModel.getVpdsWFQ());
                            
                            link.getPropertySheetHeterogeneousLink()
                                    .getItemsList()
                                    .forEach(item -> {
                                        if (item.getName().equals("C(Mbits/s)")) {
                                            item.setValue(linkModel.getCH());
                                        }
                                        if (item.getName().equals("WFQ Number")) {
                                            item.setValue(linkModel.getNvpH());
                                        }
                                    });
                            
                            link.getvPHConfig().setVphDoubleList(linkModel.getVphWFQ());
                            
                            rightPane.getChildren().add(0, link);
                            
                            DraggableNode source = null;
                            DraggableNode target = null;
                            
                            for (Node node : rightPane.getChildren()) {
                                
                                if (node.getId() == null) {
                                    continue;
                                }
                                
                                if (node.getId().equals(linkModel.getSourceId())) {
                                    source = (DraggableNode) node;
                                }
                                
                                if (node.getId().equals(linkModel.getTargetId())) {
                                    target = (DraggableNode) node;
                                }
                                
                            }
                            
                            if (source != null && target != null) {
                                link.bindEnds(source, target);
                            }
                        });
                    }
                });
    }
    
    @FXML
    private void saveDatabase(ActionEvent event) {
        
        nodes.getNodesCollection().clear();
        links.getLinksCollection().clear();
        
        rightPane.getChildren().forEach(node -> {
            if (node instanceof DraggableNode) {
                nodes.addNode((DraggableNode) node);
            } else if (node instanceof NodeLink) {
                links.addLink((NodeLink) node);
            }
        });

        // Create the custom dialog.
        Dialog<Optional<String>> dialog = new Dialog<>();
        dialog.setTitle("Topology Input Dialog");
        dialog.setHeaderText("Save Topology to Database");

// Set the icon (must be included in the project).
        dialog.setGraphic(new ImageView(this.getClass().getResource("/com/mpls/images/save-database.png").toString()));
        
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/save-database.png")));

// Set the button types.
        ButtonType saveButtonType = new ButtonType("Save", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(saveButtonType, ButtonType.CANCEL);

// Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        
        TextField label = new TextField();
        label.setPromptText("topology label");
        
        grid.add(new Label("Username:"), 0, 0);
        grid.add(label, 1, 0);

//      Enable/Disable login button depending on whether a username was entered.
        Node saveButton = dialog.getDialogPane().lookupButton(saveButtonType);
        saveButton.setDisable(true);

//      Do some validation (using the Java 8 lambda syntax).
        label.textProperty().addListener((observable, oldValue, newValue) -> {
            saveButton.setDisable(newValue.trim().isEmpty());
        });
        
        dialog.getDialogPane().setContent(grid);

//      Request focus on the username field by default.
        Platform.runLater(() -> label.requestFocus());

//      Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter((ButtonType dialogButton) -> {
            if (dialogButton == saveButtonType) {
                return Optional.of(label.getText());
            }
            return null;
        });
        
        dialog.showAndWait().ifPresent(t -> {
            consoleAppend("saving topology: " + t.get() + " to database!");
            
            Topology topo = new Topology();
            List<NodeModel> nodesModel = new LinkedList<>();
            List<LinkModel> linksModel = new LinkedList<>();
            
            topo.setLabel(t.get());
            
            nodes.getNodesCollection().forEach(node -> {
//                node.getPropertySheetNode().getItemsList().forEach(item -> {
//                    consoleAppend("name: " + item.getName() + "");
//                    consoleAppend("value: " + item.getValue() + "");
//                });

                node.getModel().setType(
                        node.getType().name()
                );

                //get node layotX rt layoutY
                node.getModel().setPoint(new Point(node.getLayoutX(), node.getLayoutY()));
                
                node.getModel().setId(
                        (String) node.getPropertySheetNode()
                                .getItemsList()
                                .stream()
                                .filter(item -> item.getName().equals("Router Id"))
                                .map(item -> item.getValue())
                                .findFirst()
                                .get()
                );
                node.getModel().setName(
                        (String) node.getPropertySheetNode()
                                .getItemsList()
                                .stream()
                                .filter(item -> item.getName().equals("Router Name"))
                                .map(item -> item.getValue())
                                .findFirst()
                                .get()
                );
                node.getModel().setIp(
                        (String) node.getPropertySheetNode()
                                .getItemsList()
                                .stream()
                                .filter(item -> item.getName().equals("Router IP"))
                                .map(item -> item.getValue())
                                .findFirst()
                                .get()
                );
                node.getModel().getBeClasses().clear();
                node.getModel().setBeClasses(
                        node.getBeClassConfig().getBeClassList()
                );
                node.getModel().getDsClasses().clear();
                node.getModel().setDsClasses(
                        node.getDsClassConfig().getDsClassList()
                );
                node.getModel().getSClasses().clear();
                node.getModel().setSClasses(
                        node.getSClassConfig().getSClassList()
                );
//                node.getModel().setBeDi(
//                        (double) node.getPropertySheetBestEffort()
//                                .getItemsList()
//                                .stream()
//                                .filter(item -> item.getName().equals("di(Mbits/s)"))
//                                .map(item -> item.getValue())
//                                .findFirst()
//                                .get()
//                );
//                node.getModel().setBeLambdai(
//                        (double) node.getPropertySheetBestEffort()
//                                .getItemsList()
//                                .stream()
//                                .filter(item -> item.getName().equals("ʎi(flows/s)"))
//                                .map(item -> item.getValue())
//                                .findFirst()
//                                .get()
//                );
//                node.getModel().setBeSigmai(
//                        (double) node.getPropertySheetBestEffort()
//                                .getItemsList()
//                                .stream()
//                                .filter(item -> item.getName().equals("σi(Mbits/s)"))
//                                .map(item -> item.getValue())
//                                .findFirst()
//                                .get()
//                );
//                node.getModel().setDivDi(
//                        (double) node.getPropertySheetDivService()
//                                .getItemsList()
//                                .stream()
//                                .filter(item -> item.getName().equals("di(Mbits/s)"))
//                                .map(item -> item.getValue())
//                                .findFirst()
//                                .get()
//                );
//                node.getModel().setDivLambdai(
//                        (double) node.getPropertySheetDivService()
//                                .getItemsList()
//                                .stream()
//                                .filter(item -> item.getName().equals("ʎi(flows/s)"))
//                                .map(item -> item.getValue())
//                                .findFirst()
//                                .get()
//                );
//                node.getModel().setDivSigmai(
//                        (double) node.getPropertySheetDivService()
//                                .getItemsList()
//                                .stream()
//                                .filter(item -> item.getName().equals("σi(Mbits/s)"))
//                                .map(item -> item.getValue())
//                                .findFirst()
//                                .get()
//                );
//                node.getModel().setDj(
//                        (double) node.getPropertySheetStreamingNode()
//                                .getItemsList()
//                                .stream()
//                                .filter(item -> item.getName().equals("dj(Mbits/s)"))
//                                .map(item -> item.getValue())
//                                .findFirst()
//                                .get()
//                );
//                node.getModel().setLambdaj(
//                        (double) node.getPropertySheetStreamingNode()
//                                .getItemsList()
//                                .stream()
//                                .filter(item -> item.getName().equals("ʎj(flows/s)"))
//                                .map(item -> item.getValue())
//                                .findFirst()
//                                .get()
//                );
//                node.getModel().setTauj(
//                        (double) node.getPropertySheetStreamingNode()
//                                .getItemsList()
//                                .stream()
//                                .filter(item -> item.getName().equals("τi(s)"))
//                                .map(item -> item.getValue())
//                                .findFirst()
//                                .get()
//                );
                nodesModel.add(node.getModel());
            });
            
            links.getLinksCollection().forEach(link -> {
                link.getModel().setType(link.getType());
                
                link.getModel().setCBE(
                        (double) link.getPropertySheetBestEffortLink()
                                .getItemsList()
                                .stream()
                                .filter(item -> item.getName().equals("C(Mbits/s)"))
                                .map(item -> item.getValue())
                                .findFirst()
                                .get()
                );
                
                link.getModel().setCDS(
                        (double) link.getPropertySheetDivServiceLink()
                                .getItemsList()
                                .stream()
                                .filter(item -> item.getName().equals("C(Mbits/s)"))
                                .map(item -> item.getValue())
                                .findFirst()
                                .get()
                );
                
                link.getModel().setNvpDS(
                        (int) link.getPropertySheetDivServiceLink()
                                .getItemsList()
                                .stream()
                                .filter(item -> item.getName().equals("WFQ Number"))
                                .map(item -> item.getValue())
                                .findFirst()
                                .get()
                );
                
                link.getModel().setVpdsWFQ(
                        link.getvPSDConfig().getVpdsDoubleList()
                );
                
                link.getModel().setCH(
                        (double) link.getPropertySheetHeterogeneousLink()
                                .getItemsList()
                                .stream()
                                .filter(item -> item.getName().equals("C(Mbits/s)"))
                                .map(item -> item.getValue())
                                .findFirst()
                                .get()
                );
                
                link.getModel().setNvpH(
                        (int) link.getPropertySheetHeterogeneousLink()
                                .getItemsList()
                                .stream()
                                .filter(item -> item.getName().equals("WFQ Number"))
                                .map(item -> item.getValue())
                                .findFirst()
                                .get()
                );
                
                link.getModel().setVphWFQ(
                        link.getvPHConfig().getVphDoubleList()
                );
                
                linksModel.add(link.getModel());
            });
            
            topo.setNodes(nodesModel);
            topo.setLinks(linksModel);

            //Test if label is used or not
            topologyService.insert(topo);
        });
    }
    
    @FXML
    private void removeFromDatabase(ActionEvent event) {
        Dialog dialog = new Dialog();
//            FontAwesomeIconView iconf = new FontAwesomeIconView(FontAwesomeIcon.FOLDER_OPEN_ALT);
//            OctIconView icon = new OctIconView(OctIcon.INFO);
//        GlyphIcon icon = GlyphsBuilder.create(FontAwesomeIconView.class).glyph(FontAwesomeIcon.REMOVE).size("3em").style("-fx-fill: linear-gradient(#70b4e5 0%, #247cbc 70%, #2c85c1 85%);").build();
        dialog.setTitle("Remove From DataBase");
        dialog.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/com/mpls/images/remove-database.png"))));
        dialog.setHeaderText("Remove Topology from Database");
        
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/remove-database.png")));

        // Set the button types.
        ButtonType loadButtonType = new ButtonType("Remove", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loadButtonType, ButtonType.CANCEL);

        // Enable/Disable login button depending on whether a username was entered.
        Node loadButton = dialog.getDialogPane().lookupButton(loadButtonType);
        loadButton.setDisable(true);
        
        ListView listView = new ListView();
        
        topologyService.findAll().forEach(topo -> {
            listView.getItems().add(topo.getLabel());
        });

//            GridPane.setVgrow(listView, Priority.ALWAYS);
//            GridPane.setHgrow(listView, Priority.ALWAYS);
        GridPane listContent = new GridPane();
        listContent.setMaxWidth(Double.MAX_VALUE);
        listContent.add(listView, 0, 0);

        // Do some validation (using the Java 8 lambda syntax).
        listView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//                System.out.println("ListView selection changed from oldValue = "
//                        + oldValue + " to newValue = " + newValue);
            loadButton.setDisable(false);
        });

        // Set expandable Exception into the dialog pane.
        dialog.getDialogPane().setContent(listContent);
        
        dialog.showAndWait()
                .filter(response -> response == loadButtonType)
                .ifPresent(response -> {
                    Topology topo = topologyService.findByLabel((String) listView.getSelectionModel().getSelectedItem());
                    
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Confirmation Dialog");
                    GlyphIcon icon = GlyphsBuilder.create(OctIconView.class).glyph(OctIcon.INFO).size("3em").style("-fx-fill: linear-gradient(#70b4e5 0%, #247cbc 70%, #2c85c1 85%);").build();
                    alert.setGraphic(icon);
                    alert.setHeaderText("Remove Topology Confirmation Dialog");
                    alert.setContentText("Are you ok with removing topology from Database?");
                    
                    Stage stage1 = (Stage) alert.getDialogPane().getScene().getWindow();
                    stage1.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/info.png")));
                    
                    alert.showAndWait()
                            .filter(result -> result.getButtonData() == ButtonData.OK_DONE)
                            .ifPresent(result -> {
                                consoleAppend("removing topo: " + listView.getSelectionModel().getSelectedItem() + " from database!");
                                topologyService.remove(topo);
                                clear(event);
                            });
                    
                });
    }
    
    @FXML
    private void changePassword(ActionEvent event) {
        Dialog dialog = new Dialog();
        dialog.setTitle("Password Change");
        dialog.setHeaderText("Change your password!");

        // Set the icon (must be included in the project).
        GlyphIcon icon = GlyphsBuilder.create(FontAwesomeIconView.class).glyph(FontAwesomeIcon.UNLOCK_ALT).size("3em").style("-fx-fill: linear-gradient(#70b4e5 0%, #247cbc 70%, #2c85c1 85%);").build();
        dialog.setGraphic(icon);
        
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/unlock.png")));

// Set the button types.
        ButtonType changeButtonType = new ButtonType("Change", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(changeButtonType, ButtonType.CANCEL);

// Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        
        PasswordField oldPassword = new PasswordField();
        oldPassword.setPromptText("Old Password");
        PasswordField password = new PasswordField();
        password.setPromptText("New Password");
        PasswordField confirmPassword = new PasswordField();
        confirmPassword.setPromptText("Confirm Password");
        
        grid.add(new Label("Old Password:"), 0, 0);
        grid.add(oldPassword, 1, 0);
        grid.add(new Label("New Password:"), 0, 1);
        grid.add(password, 1, 1);
        grid.add(new Label("Conform Password:"), 0, 2);
        grid.add(confirmPassword, 1, 2);

// Enable/Disable login button depending on whether a username was entered.
        Button changeButton = (Button) dialog.getDialogPane().lookupButton(changeButtonType);
        changeButton.setDisable(true);
        
        changeButton.addEventFilter(ActionEvent.ACTION, e -> {
            if (!validateAndStore(oldPassword, password, confirmPassword)) {
                GridPane g = (GridPane) dialog.getDialogPane().lookup(".header-panel");
                Label label = (Label) g.lookup(".label");
                g.setStyle("-fx-background-color: #FF6B6B; -fx-font-style: italic;");
                label.setTextFill(Color.WHITE);
                label.setStyle("-fx-font-size: 18px;");
                label.setText("Password or Confirm Password are wrong!");
                e.consume();
            }
        });

// Do some validation (using the Java 8 lambda syntax).
        oldPassword.textProperty().addListener((observable, oldValue, newValue) -> {
            changeButton.setDisable(newValue.trim().isEmpty());
        });
        
        dialog.getDialogPane().setContent(grid);

// Request focus on the username field by default.
        Platform.runLater(() -> oldPassword.requestFocus());
        dialog.showAndWait()
                .filter(response -> response == changeButtonType)
                .ifPresent(response -> {
                    User user = userService.findByLogin("admin");
                    user.setPassword(new BCryptPasswordEncoder().encode(password.getText()));
                    
                    userService.edit(user);
                });
    }
    
    @FXML
    private void clear(ActionEvent event) {
        rightPane.getChildren().clear();
        nodes.getNodesCollection().clear();
        links.getLinksCollection().clear();
        console.setText("");
    }
    
    @FXML
    private void close(ActionEvent event) {
        System.out.println("closing2.........");
        Platform.exit();
    }
    
    private void bestEffortSimulation(ActionEvent event) throws IOException {
        nodes.getNodesCollection().clear();
        links.getLinksCollection().clear();
        
        rightPane.getChildren().forEach(node -> {
            if (node instanceof DraggableNode) {
                String label = (String) ((DraggableNode) node).getPropertySheetNode()
                        .getItemsList()
                        .stream()
                        .filter(item -> item.getName().equals("Router Id"))
                        .map(item -> item.getValue())
                        .findFirst()
                        .get();
                ((DraggableNode) node).setLabel(label);
                nodes.addNode((DraggableNode) node);
            } else if (node instanceof NodeLink) {
                links.addLink((NodeLink) node);
            }
        });

//        nodes.getNodesCollection().forEach(node -> {
//            node.getPropertySheetNode().getItemsList().forEach(item -> {
//                consoleAppend("name: " + item.getName() + "");
//                consoleAppend("value: " + item.getValue() + "");
//            });
//        });
        Dialog dialog = new Dialog();
        GlyphIcon play = GlyphsBuilder.create(FontAwesomeIconView.class).glyph(FontAwesomeIcon.PLAY_CIRCLE).size("3em").style("-fx-fill: linear-gradient(#70b4e5 0%, #247cbc 70%, #2c85c1 85%);").build();
        dialog.setTitle("Create a Path from Source Node (LER Router)!");
//        dialog.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/com/mpls/images/remove-database.png"))));
//        dialog.setGraphic(play);
//        dialog.setHeaderText("Create a Path, and Run Simulation!");

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/play.png")));

        // Set the button types.
//        ButtonType loadButtonType = new ButtonType("Run", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

        // Enable/Disable login button depending on whether a username was entered.
//        Node runButton = dialog.getDialogPane().lookupButton(loadButtonType);
//        runButton.setDisable(true);
//        AnchorPane pane = (AnchorPane) FXMLLoader.load(getClass().getResource("/com/mpls/fxml/paths.fxml"));
//        Paths paths = new Paths(nodes);
        BEClassPath bePath = new BEClassPath(nodes, links);
        GridPane listContent = new GridPane();
        listContent.setMaxWidth(Double.MAX_VALUE);
        listContent.add(bePath, 0, 0);

        // Do some validation (using the Java 8 lambda syntax).
//        paths.getFinalList().getItems().addListener((ListChangeListener.Change<? extends NodePathModel> c) -> {
//            runButton.setDisable(paths.getFinalList().getItems().isEmpty());
//        });
        // Set expandable Exception into the dialog pane.
        dialog.getDialogPane().setContent(listContent);
        
        dialog.showAndWait()
                .ifPresent(response -> {
                    //TODO
                    try {
                        reportModel.addModel(rightPane);
                        bestEffortProcesingService.setBeClassesMap(bePath.getBeClassesMap());
                        reportModel.setBeClassesMap(bePath.getBeClassesMap());
                        Map<BEClass, Double> bestEffortDebitMoyen = bestEffortProcesingService.processingBestEffort();
                        reportModel.setBestEffortDebitMoyen(bestEffortDebitMoyen);
                        beConsoleSimulation();
                        showBestEffortReport(event);
                    } catch (IOException | SubTetaException ex) {
                        consoleAppend(ex.getMessage());
                        Logger.getLogger(ViewController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
    }
    
    @FXML
    private void runBestEffort(ActionEvent event) throws IOException {
        bestEffortSimulation(event);
    }
    
    @FXML
    private void runDivService(ActionEvent event) throws IOException {
        
    }
    
    @FXML
    private void runHeterogeneous(ActionEvent event) throws IOException {
        
    }
    
    @FXML
    private void menuElasticBestEffort(ActionEvent event) throws IOException {
        bestEffortSimulation(event);
    }
    
    @FXML
    private void menuDivService(ActionEvent event) throws IOException {
        
    }
    
    @FXML
    private void menuHeterogeneous(ActionEvent event) throws IOException {
        
    }
    
    @FXML
    private void showBestEffortReport(ActionEvent event) throws IOException {
        Dialog dialog = new Dialog();
        Window window = dialog.getDialogPane().getScene().getWindow();
        window.setOnCloseRequest(e -> {
            dialog.close();
        });
        dialog.initModality(Modality.NONE);
        
        dialog.setTitle("Show Report!");
        
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/icon-pdf.png")));

//        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);
        //Show Report Viewer
        PdfViewer viewer = new PdfViewer();
        PdfReport report = new PdfReport();
        
        BorderPane pane = new BorderPane();
        Scene scene = (Scene) dialog.getDialogPane().getScene();
        scene.setRoot(pane);

        // add viewer content pane
        viewer.createViewer(pane);
//        pane.setPrefSize(1025, 600);
        pane.setPrefSize(Screen.getPrimary().getVisualBounds().getWidth() - 100, Screen.getPrimary().getVisualBounds().getHeight() - 100);
        viewer.createResizeListeners(scene);
        viewer.openDocument(report.createBestEffortReport(reportModel));
        
        dialog.getDialogPane().setContent(pane);
        dialog.showAndWait();
    }
    
    private boolean validateAndStore(PasswordField oldPassword, PasswordField password, PasswordField confirmPassword) {
        return new BCryptPasswordEncoder().matches(oldPassword.getText(), userService.findByLogin("admin").getPassword()) && !password.getText().isEmpty() && !confirmPassword.getText().isEmpty() && password.getText().equals(confirmPassword.getText());
    }
    
    private void beConsoleSimulation() {
        reportModel.getBeClassesMap().forEach((k, v) -> {
            consoleAppend(k.getLabel() + ":");
            sb = new StringBuilder();
            consoleAppend("nodes: ");
            v.getPathNodesList().forEach(node -> {
                sb.append(" -> ")
                        .append(node.getLabel())
                        .append(" (").append("d(Mbits/s): ").append(k.getD()).append(" - ʎ(flows/s): ").append(k.getLambda()).append(" - σ(Mbits/s): ").append(k.getSigma()).append(")");
                consoleAppend(sb.toString());
                sb = new StringBuilder();
            });
            
            consoleAppend("links: ");
            v.getPathLinksList().forEach(link -> {
                sb.append(" -> ")
                        .append(link.getModel().getUid())
                        .append(" (").append("C(Mbits/s): ").append(link.getModel().getCBE()).append(")");
                consoleAppend(sb.toString());
                sb = new StringBuilder();
            });
            
        });
        
        consoleAppend("");
        
        reportModel.getBestEffortDebitMoyen().forEach((k, v) -> {
            consoleAppend(k.getLabel() + "  debit moyen: " + v);
        });
    }
}
