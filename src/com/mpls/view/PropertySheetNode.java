/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.model.node.NodePropertyItem;
import com.mpls.model.node.NodesDataMap;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.control.PropertySheet;

/**
 *
 * @author Mensi Kais
 */
public class PropertySheetNode extends VBox {

    private final PropertySheet propertySheet;
    private ObservableList<PropertySheet.Item> itemsList;

    private NodesDataMap nodesDataMap;

    public PropertySheetNode(NodesDataMap nodesData) {
        this.nodesDataMap = nodesData;
        itemsList = FXCollections.observableArrayList();
        nodesData.getNodesMap().keySet().forEach((key) -> {
            NodePropertyItem nodePropertyItem = new NodePropertyItem();
            nodePropertyItem.setNodesData(nodesData);
            nodePropertyItem.setKey(key);
            itemsList.add(nodePropertyItem);
        });

        propertySheet = new PropertySheet(itemsList);
        VBox.setVgrow(propertySheet, Priority.ALWAYS);
        getChildren().add(propertySheet);
    }

    public PropertySheet getPropertySheet() {
        return propertySheet;
    }

    public ObservableList<PropertySheet.Item> getItemsList() {
        return itemsList;
    }

    public NodesDataMap getNodesDataMap() {
        return nodesDataMap;
    }

}
