/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.model.BestEffortPathCollection;
import com.mpls.model.LinksCollection;
import com.mpls.model.NodePathModel;
import com.mpls.model.NodesCollection;
import com.mpls.model.node.BEClass;
import com.mpls.model.path.BEPathNodeView;
import de.jensd.fx.glyphs.GlyphIcon;
import de.jensd.fx.glyphs.GlyphsBuilder;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Mensi Kais
 */
public class BEClassPath extends BorderPane implements Initializable {
    
    private FXMLLoader loader;
    private NodesCollection nodes;
    private LinksCollection links;
    private String linkType = "";

    private Map<BEClass, BestEffortPathCollection> beClassesMap = new LinkedHashMap<>();

    @FXML
    private ListView<BEPathNodeView> nodesList;
    @FXML
    private Label nodeLabel;
    @FXML
    private ListView<BEClass> classesList;
    @FXML
    private Button btnClassPath;

    public BEClassPath(NodesCollection nodes, LinksCollection links) {
        this.nodes = nodes;
        this.links = links;
        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/be-class-path.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        nodes.getNodesCollection().forEach(node -> {
            BEPathNodeView nodeView = new BEPathNodeView();
            nodeView.setNodeLabel(node.getLabel());
            nodeView.setNode(node);
            nodesList.getItems().add(nodeView);
        });

//        nodes.getNodesCollection().forEach(node -> {
//            node.getBeClassConfig().getBeClassList().forEach(System.out::println);
//        });
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnClassPath.setDisable(true);
        classesList.getItems().addListener((ListChangeListener.Change<? extends BEClass> c) -> {
            btnClassPath.setDisable(classesList.getItems().isEmpty());
        });
    }

    @FXML
    private void selectSourceNode(ActionEvent event) {
        nodeLabel.setText(nodesList.getSelectionModel().getSelectedItem().getNodeLabel());
        classesList.getItems().clear();
        classesList.getItems().addAll(nodesList.getSelectionModel().getSelectedItem().getNode().getBeClassConfig().getBeClassList());

    }

    @FXML
    private void createClassPath(ActionEvent event) {

        BEClass beClass = classesList.getSelectionModel().getSelectedItem();

        int i = classesList.getItems().indexOf(beClass);

        switch (i) {
            case 0:
                linkType = NodeLink.BLEU;
                break;
            case 1:
                linkType = NodeLink.GREEN;
                break;
            case 2:
                linkType = NodeLink.RED;
                break;
            default:
                linkType = NodeLink.BLACK;
        }

        Dialog dialog = new Dialog();
        GlyphIcon play = GlyphsBuilder.create(FontAwesomeIconView.class).glyph(FontAwesomeIcon.PLAY_CIRCLE).size("3em").style("-fx-fill: linear-gradient(#70b4e5 0%, #247cbc 70%, #2c85c1 85%);").build();
        dialog.setTitle("Create a Class Path from Source Node (LER Router)!");
//        dialog.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/com/mpls/images/remove-database.png"))));
//        dialog.setGraphic(play);
//        dialog.setHeaderText("Create a Path, and Run Simulation!");

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/play.png")));

        // Set the button types.
        ButtonType runButtonType = new ButtonType("Add", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(runButtonType);

        // Enable/Disable login button depending on whether a username was entered.
        Node runButton = dialog.getDialogPane().lookupButton(runButtonType);
        runButton.setDisable(true);
//        AnchorPane pane = (AnchorPane) FXMLLoader.load(getClass().getResource("/com/mpls/fxml/paths.fxml"));
        Paths paths = new Paths(nodes);
//        BEClassPath path = new BEClassPath(nodes);
        GridPane listContent = new GridPane();
        listContent.setMaxWidth(Double.MAX_VALUE);
        listContent.add(paths, 0, 0);

        // Do some validation (using the Java 8 lambda syntax).
        paths.getFinalList().getItems().addListener((ListChangeListener.Change<? extends NodePathModel> c) -> {
            runButton.setDisable(paths.getFinalList().getItems().isEmpty());
        });
        // Set expandable Exception into the dialog pane.
        dialog.getDialogPane().setContent(listContent);

        dialog.showAndWait()
                .ifPresent(response -> {
                    //TODO
                    BestEffortPathCollection beCollection = paths.createPath(links, linkType);
                    beClassesMap.put(beClass, beCollection);

//                    beClassesMap.forEach((k, v) -> {
//                        System.out.println(k.getLabel() + ":");
//                        v.getPathNodesList().forEach(node -> {
//                            System.out.println(node.getLabel());
//                        });
//                        v.getPathLinksList().forEach(link -> {
//                            System.out.println(link.getModel().toString());
//                        });
//                    });
                });
    }

    public Map<BEClass, BestEffortPathCollection> getBeClassesMap() {
        return beClassesMap;
    }

}
