/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.view.link.PropertySheetBestEffortLink;
import com.mpls.view.link.PropertySheetDivServiceLink;
import com.mpls.view.link.PropertySheetHeterogeneousLink;
import com.mpls.view.link.VPDSConfig;
import com.mpls.view.link.VPHConfig;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;

/**
 * FXML Controller class
 *
 * @author kmens
 */
public class LinkConfig extends AnchorPane implements Initializable {

    private FXMLLoader loader;

    private PropertySheetBestEffortLink propertySheetBestEffortLink;
    private PropertySheetDivServiceLink propertySheetDivServiceLink;
    private PropertySheetHeterogeneousLink propertySheetHeterogeneousLink;

    private PopOver popOver;

    private VPDSConfig vPSDConfig;
    private VPHConfig vPHConfig;

    @FXML
    private BorderPane bestEffortPane;
    @FXML
    private BorderPane divServicePane;
    @FXML
    private BorderPane heterogeneousPane;

    public LinkConfig(
            PropertySheetBestEffortLink propertySheetBestEffortLink,
            PropertySheetDivServiceLink propertySheetDivServiceLink,
            PropertySheetHeterogeneousLink propertySheetHeterogeneousLink,
            VPDSConfig vPSDConfig,
            VPHConfig vPHConfig,
            PopOver popOver
    ) {

        this.propertySheetBestEffortLink = propertySheetBestEffortLink;
        this.propertySheetDivServiceLink = propertySheetDivServiceLink;
        this.propertySheetHeterogeneousLink = propertySheetHeterogeneousLink;

        this.vPSDConfig = vPSDConfig;
        this.vPHConfig = vPHConfig;

        this.popOver = popOver;

        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/link-config.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bestEffortPane.setCenter(propertySheetBestEffortLink);
        divServicePane.setCenter(propertySheetDivServiceLink);
        heterogeneousPane.setCenter(propertySheetHeterogeneousLink);
    }

    @FXML
    private void addDSVirtualPath(ActionEvent event) {
        Dialog dialog = new Dialog();
        dialog.initOwner(popOver.getScene().getWindow());
        dialog.setTitle("DivService Virtual Path Config");

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/traffic.png")));

        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

        dialog.getDialogPane().setContent(vPSDConfig);

        dialog.showAndWait();
    }

    @FXML
    private void addHVirtualPath(ActionEvent event) {
        Dialog dialog = new Dialog();
        dialog.initOwner(popOver.getScene().getWindow());
        dialog.setTitle("Heterogeneous Virtual Path Config");

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/traffic.png")));

        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

        dialog.getDialogPane().setContent(vPHConfig);

        dialog.showAndWait();
    }

}
