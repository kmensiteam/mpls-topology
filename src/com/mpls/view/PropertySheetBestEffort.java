/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.model.node.BeDataMap;
import com.mpls.model.node.BePropertyItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.controlsfx.control.PropertySheet;

/**
 *
 * @author Mensi Kais
 */
public class PropertySheetBestEffort extends VBox {

    private final PropertySheet propertySheet;
    ObservableList<PropertySheet.Item> itemsList;

    public PropertySheetBestEffort(BeDataMap beData) {
        itemsList = FXCollections.observableArrayList();
        beData.getNodesMap().keySet().forEach((key) -> {
            BePropertyItem elasticPropertyItem = new BePropertyItem();
            elasticPropertyItem.setElasticData(beData);
            elasticPropertyItem.setKey(key);
            itemsList.add(elasticPropertyItem);
        });

        propertySheet = new PropertySheet(itemsList);
        VBox.setVgrow(propertySheet, Priority.ALWAYS);
        getChildren().add(propertySheet);
    }

    public ObservableList<PropertySheet.Item> getItemsList() {
        return itemsList;
    }

}
