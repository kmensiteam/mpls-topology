/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view.node;

import com.mpls.model.node.SClass;
import com.mpls.model.node.StreamingDataMap;
import com.mpls.view.PropertySheetStreamingNode;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;

/**
 * FXML Controller class
 *
 * @author Mensi Kais
 */
public class SClassConfig extends BorderPane implements Initializable {

    private FXMLLoader loader;

    @FXML
    private ListView<SClass> listClass;
    @FXML
    private TextField inputClass;
    @FXML
    private Button btnConfigClass;

    private PopOver popOver;

    private List<SClass> sClassList = new ArrayList<>();

    public SClassConfig(
            PopOver popOver
    ) {

        this.popOver = popOver;

        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/class-config.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnConfigClass.setDisable(true);
        listClass.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//                System.out.println("ListView selection changed from oldValue = "
//                        + oldValue + " to newValue = " + newValue);
            btnConfigClass.setDisable(false);
        });
    }

    @FXML
    private void addClass(ActionEvent event) {
        SClass sClass = new SClass(inputClass.getText());
        if (!listClass.getItems().contains(sClass)) {
            listClass.getItems().add(sClass);
            inputClass.clear();
        }
    }

    @FXML
    private void deleteClass(ActionEvent event) {
        listClass.getItems().remove(listClass.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void configClass(ActionEvent event) {

        SClass sClass = listClass.getSelectionModel().getSelectedItem();
        StreamingDataMap streamingDataMap = new StreamingDataMap(sClass);
        PropertySheetStreamingNode propertySheetStreamingNode = new PropertySheetStreamingNode(streamingDataMap);

        Dialog dialog = new Dialog();
        dialog.initOwner(popOver.getScene().getWindow());
        dialog.setTitle("Streaming Config " + sClass.getLabel());

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/traffic.png")));

        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

        dialog.getDialogPane().setContent(propertySheetStreamingNode);

        dialog
                .showAndWait()
                .ifPresent(result -> {
                    sClass.setD(
                            (double) propertySheetStreamingNode
                                    .getItemsList()
                                    .stream()
                                    .filter(item -> item.getName().equals("dj(Mbits/s)"))
                                    .map(item -> item.getValue())
                                    .findFirst()
                                    .get()
                    );

                    sClass.setLambda(
                            (double) propertySheetStreamingNode
                                    .getItemsList()
                                    .stream()
                                    .filter(item -> item.getName().equals("ʎj(flows/s)"))
                                    .map(item -> item.getValue())
                                    .findFirst()
                                    .get()
                    );

                    sClass.setTau(
                            (double) propertySheetStreamingNode
                                    .getItemsList()
                                    .stream()
                                    .filter(item -> item.getName().equals("τi(s)"))
                                    .map(item -> item.getValue())
                                    .findFirst()
                                    .get()
                    );

                });
    }

    public List<SClass> getSClassList() {
        listClass.getItems().forEach(i -> {
            sClassList.add(i);
        });
        return sClassList;
    }

    public void setSClassList(List<SClass> sClassList) {
        this.sClassList = sClassList;
        listClass.getItems().clear();
        listClass.getItems().addAll(sClassList);
    }

}
