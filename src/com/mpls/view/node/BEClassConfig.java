/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view.node;

import com.mpls.model.node.BEClass;
import com.mpls.model.node.BeDataMap;
import com.mpls.view.PropertySheetBestEffort;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;

/**
 * FXML Controller class
 *
 * @author Mensi Kais
 */
public class BEClassConfig extends BorderPane implements Initializable {

    private FXMLLoader loader;

    @FXML
    private ListView<BEClass> listClass;
    @FXML
    private TextField inputClass;
    @FXML
    private Button btnConfigClass;

    private PopOver popOver;

    private List<BEClass> beClassList = new ArrayList<>();

    public BEClassConfig(
            PopOver popOver
    ) {
        this.popOver = popOver;

        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/class-config.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnConfigClass.setDisable(true);
        listClass.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//                System.out.println("ListView selection changed from oldValue = "
//                        + oldValue + " to newValue = " + newValue);
            btnConfigClass.setDisable(false);
        });
    }

    @FXML
    private void addClass(ActionEvent event) {
        BEClass beClass = new BEClass(inputClass.getText());
        if (!listClass.getItems().contains(beClass)) {
            listClass.getItems().add(beClass);
            inputClass.clear();
        }
    }

    @FXML
    private void deleteClass(ActionEvent event) {
        listClass.getItems().remove(listClass.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void configClass(ActionEvent event) {

        BEClass beClass = listClass.getSelectionModel().getSelectedItem();
        BeDataMap bestEffortDataMap = new BeDataMap(beClass);
        PropertySheetBestEffort propertySheetBestEffort = new PropertySheetBestEffort(bestEffortDataMap);

        Dialog dialog = new Dialog();
        dialog.initOwner(popOver.getScene().getWindow());
        dialog.setTitle("Best Effort Config " + beClass.getLabel());

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/traffic.png")));

        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

        dialog.getDialogPane().setContent(propertySheetBestEffort);

        dialog
                .showAndWait()
                .ifPresent(result -> {
                    System.out.println(beClass);
//                    propertySheetBestEffort.getItemsList().forEach(item -> {
//                        System.out.println("name: " + item.getName() + "");
//                        System.out.println("value: " + item.getValue() + "");
//
//                    });
                    beClass.setD(
                            (double) propertySheetBestEffort
                                    .getItemsList()
                                    .stream()
                                    .filter(item -> item.getName().equals("di(Mbits/s)"))
                                    .map(item -> item.getValue())
                                    .findFirst()
                                    .get()
                    );

                    beClass.setLambda(
                            (double) propertySheetBestEffort
                                    .getItemsList()
                                    .stream()
                                    .filter(item -> item.getName().equals("ʎi(flows/s)"))
                                    .map(item -> item.getValue())
                                    .findFirst()
                                    .get()
                    );

                    beClass.setSigma(
                            (double) propertySheetBestEffort
                                    .getItemsList()
                                    .stream()
                                    .filter(item -> item.getName().equals("σi(Mbits/s)"))
                                    .map(item -> item.getValue())
                                    .findFirst()
                                    .get()
                    );

                });
    }

    public List<BEClass> getBeClassList() {
        beClassList.clear();
        listClass.getItems().forEach(i -> {
            beClassList.add(i);
        });
        return beClassList;
    }

    public void setBeClassList(List<BEClass> beClassList) {
        this.beClassList = beClassList;
        listClass.getItems().clear();
        listClass.getItems().addAll(beClassList);
    }

}
