/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view.node;

import com.mpls.model.node.DSClass;
import com.mpls.model.node.DivDataMap;
import com.mpls.view.PropertySheetDivService;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;

/**
 * FXML Controller class
 *
 * @author Mensi Kais
 */
public class DSClassConfig extends BorderPane implements Initializable {

    private FXMLLoader loader;

    @FXML
    private ListView<DSClass> listClass;
    @FXML
    private TextField inputClass;
    @FXML
    private Button btnConfigClass;

    private PopOver popOver;

    private List<DSClass> dsClassList = new ArrayList<>();

    public DSClassConfig(
            PopOver popOver
    ) {

        this.popOver = popOver;

        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/class-config.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnConfigClass.setDisable(true);
        listClass.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//                System.out.println("ListView selection changed from oldValue = "
//                        + oldValue + " to newValue = " + newValue);
            btnConfigClass.setDisable(false);
        });
    }

    @FXML
    private void addClass(ActionEvent event) {
        DSClass dsClass = new DSClass(inputClass.getText());
        if (!listClass.getItems().contains(dsClass)) {
            listClass.getItems().add(dsClass);
            inputClass.clear();
        }
    }

    @FXML
    private void deleteClass(ActionEvent event) {
        listClass.getItems().remove(listClass.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void configClass(ActionEvent event) {

        DSClass dsClass = listClass.getSelectionModel().getSelectedItem();
        DivDataMap divServiceDataMap = new DivDataMap(dsClass);
        PropertySheetDivService propertySheetDivService = new PropertySheetDivService(divServiceDataMap);

        Dialog dialog = new Dialog();
        dialog.initOwner(popOver.getScene().getWindow());
        dialog.setTitle("DivService Config " + dsClass.getLabel());

        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/traffic.png")));

        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);

        dialog.getDialogPane().setContent(propertySheetDivService);

        dialog
                .showAndWait()
                .ifPresent(result -> {
                    dsClass.setD(
                            (double) propertySheetDivService
                                    .getItemsList()
                                    .stream()
                                    .filter(item -> item.getName().equals("di(Mbits/s)"))
                                    .map(item -> item.getValue())
                                    .findFirst()
                                    .get()
                    );

                    dsClass.setLambda(
                            (double) propertySheetDivService
                                    .getItemsList()
                                    .stream()
                                    .filter(item -> item.getName().equals("ʎi(flows/s)"))
                                    .map(item -> item.getValue())
                                    .findFirst()
                                    .get()
                    );

                    dsClass.setSigma(
                            (double) propertySheetDivService
                                    .getItemsList()
                                    .stream()
                                    .filter(item -> item.getName().equals("σi(Mbits/s)"))
                                    .map(item -> item.getValue())
                                    .findFirst()
                                    .get()
                    );

                });
    }

    public List<DSClass> getDsClassList() {
        listClass.getItems().forEach(i -> {
            dsClassList.add(i);
        });
        return dsClassList;
    }

    public void setDsClassList(List<DSClass> dsClassList) {
        this.dsClassList = dsClassList;
        listClass.getItems().clear();
        listClass.getItems().addAll(dsClassList);
    }

}
