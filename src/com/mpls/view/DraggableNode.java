package com.mpls.view;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mpls.model.node.BeDataMap;
import com.mpls.model.node.DivDataMap;
import com.mpls.model.NodeModel;
import com.mpls.model.node.BEClass;
import com.mpls.model.node.DSClass;
import com.mpls.model.node.NodesDataMap;
import com.mpls.model.node.StreamingDataMap;
import com.mpls.model.node.SClass;
import com.mpls.view.node.BEClassConfig;
import com.mpls.view.node.DSClassConfig;
import com.mpls.view.node.SClassConfig;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;
import java.util.stream.Collectors;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import org.controlsfx.control.PopOver;

/**
 * FXML Controller class
 *
 * @author Mensi Kais
 */
public class DraggableNode extends AnchorPane {

    private FXMLLoader loader;

    @FXML
    private Label closeButton;
    @FXML
    private Label titleBar;

    @FXML
    private AnchorPane nodeBody;
    @FXML
    private AnchorPane leftLinkHandle;
    @FXML
    private AnchorPane rightLinkHandle;

    private String uid = UUID.randomUUID().toString();
    private String label = "";

    private PopOver popOver;
    private PropertySheetNode propertySheetNode;
    private BEClassConfig beClassConfig;
    private DSClassConfig dsClassConfig;
    private SClassConfig sClassConfig;
    private NodeConfig nodeConfig;

    private NodesDataMap nodesData;
    private NodeModel model = new NodeModel();

    private NodeLink dragLink = null;
    private AnchorPane rightPane = null;
    private final List<String> linkIds = new ArrayList<>();
    private Point2D dragOffset = new Point2D(0.0, 0.0);
//    private Point2D location;
    private DragIconType type;

    private EventHandler<DragEvent> contextDragOver;
    private EventHandler<DragEvent> contextDragDropped;

    private EventHandler<MouseEvent> linkHandleDragDetected;
    private EventHandler<DragEvent> linkHandleDragDropped;
    private EventHandler<DragEvent> contextLinkDragOver;
    private EventHandler<DragEvent> contextLinkDragDropped;

    private final DraggableNode self;

    public DraggableNode() {
        self = this;

        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/draggable-node.fxml"));
            loader.setRoot(self);
            loader.setController(self);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        //provide a universally unique identifier for this object
        setId(uid);
    }

    public void initialize() {
        getStyleClass().add("dragicon");
        getStyleClass().add("icon-color");
        this.setOpacity(0.9);

        buildNodeDragHandlers();
        buildLinkDragHandlers();

        leftLinkHandle.setOnDragDetected(linkHandleDragDetected);
        rightLinkHandle.setOnDragDetected(linkHandleDragDetected);

        leftLinkHandle.setOnDragDropped(contextLinkDragDropped);
        leftLinkHandle.setOnDragDropped(linkHandleDragDropped);

        dragLink = new NodeLink();
        dragLink.setVisible(false);

        parentProperty().addListener((observable, oldValue, newValue) -> {
            rightPane = (AnchorPane) getParent();
        });

        model.setUid(uid);
        nodesData = new NodesDataMap(model);

        popOver = new PopOver();
        propertySheetNode = new PropertySheetNode(nodesData);  
        beClassConfig = new BEClassConfig(popOver);
        dsClassConfig = new DSClassConfig(popOver);
        sClassConfig = new SClassConfig(popOver);

        nodeConfig = new NodeConfig(
                propertySheetNode,
                beClassConfig,
                dsClassConfig,
                sClassConfig,
                popOver
        );
    }

    @FXML
    private void showPropertySheet(MouseEvent event) {
        popOver.setDetached(true);
//        popOver.setAutoHide(false);
        popOver.setTitle("Info");
//        popOver.setContentNode(propertySheetNode);
        popOver.setContentNode(nodeConfig);
        popOver.show(nodeBody);
    }

    public NodeModel getModel() {
        return model;
    }

    public void setModel(NodeModel model) {
        this.model = model;
    }

    public PropertySheetNode getPropertySheetNode() {
        return propertySheetNode;
    }

    public BEClassConfig getBeClassConfig() {
        return beClassConfig;
    }

    public DSClassConfig getDsClassConfig() {
        return dsClassConfig;
    }

    public SClassConfig getSClassConfig() {
        return sClassConfig;
    }

    public void registerLink(String linkId) {
        linkIds.add(linkId);
    }

    public void relocateToPoint(Point2D p) {
        //relocates the object to a point that has been converted to
        //scene coordinates
        Point2D localCoords = getParent().sceneToLocal(p);
        relocate(
                (int) (localCoords.getX() - dragOffset.getX()),
                (int) (localCoords.getY() - dragOffset.getY())
        );

//        location = new Point2D(localCoords.getX() - dragOffset.getX(), localCoords.getY() - dragOffset.getY());
    }

    public List<String> getLinkIds() {
        return linkIds;
    }

    public void addLabel() {
        String label;
        switch (this.type) {

            case LSR:
                label = "LSR";
                break;

            case LER:
                label = "LER";
                break;

            default:
                label = "";
                break;
        }
        titleBar.setText(label);
    }

    public String getLabel() {
        if (label.isEmpty()) {
            switch (this.type) {

                case LSR:
                    label = "LSR";
                    break;

                case LER:
                    label = "LER";
                    break;

                default:
                    label = "";
                    break;
            }
        }

        return label;
    }

    public void setLabel(String label) {
//        switch (this.type) {
//
//            case LSR:
//                label = "LSR-" + label;
//                break;
//
//            case LER:
//                label = "LER-" + label;
//                break;
//
//            default:
//                label = "";
//                break;
//        }
        this.label = label;
        titleBar.setText(label);
    }

    public DragIconType getType() {
        return type;
    }

    public void setType(DragIconType type) {
        this.type = type;

        getStyleClass().clear();
        getStyleClass().add("dragicon");
        getStyleClass().add("icon-color");

        switch (this.type) {

            case LSR:
                getStyleClass().add("icon-router-lsr");
                break;

            case LER:
                getStyleClass().add("icon-router-ler");
                break;

            default:
                break;
        }

        addLabel();
        model.setType(type.name());
    }

    public void buildNodeDragHandlers() {

        contextDragOver = (DragEvent event) -> {
            event.acceptTransferModes(TransferMode.ANY);
            relocateToPoint(new Point2dSerial(event.getSceneX(), event.getSceneY()));

            event.consume();
        } //dragover to handle node dragging in the right pane view
                ;

        //dragdrop for node dragging
        contextDragDropped = (DragEvent event) -> {
            getParent().setOnDragOver(null);
            getParent().setOnDragDropped(null);

            event.setDropCompleted(true);

            event.consume();
        };

        //close button click
        closeButton.setOnMouseClicked((MouseEvent event) -> {
            AnchorPane parent1 = (AnchorPane) self.getParent();
            parent1.getChildren().remove(self);
            //iterate each link id connected to this node
            //find it's corresponding component in the right-hand
            //AnchorPane and delete it.
            //Note:  other nodes connected to these links are not being
            //notified that the link has been removed.
            for (ListIterator<String> iterId = linkIds.listIterator(); iterId.hasNext();) {
                String id1 = iterId.next();
                for (ListIterator<Node> iterNode = parent1.getChildren().listIterator(); iterNode.hasNext();) {
                    Node node = iterNode.next();
                    if (node.getId() == null) {
                        continue;
                    }
                    if (node.getId().equals(id1)) {
                        iterNode.remove();
                    }
                }
                iterId.remove();
            }
        });

        //drag detection for node dragging
        titleBar.setOnDragDetected((MouseEvent event) -> {
            getParent().setOnDragOver(null);
            getParent().setOnDragDropped(null);

            getParent().setOnDragOver(contextDragOver);
            getParent().setOnDragDropped(contextDragDropped);

            //begin drag ops
            dragOffset = new Point2D(event.getX(), event.getY());

            relocateToPoint(
                    new Point2D(event.getSceneX(), event.getSceneY())
            );

            ClipboardContent content = new ClipboardContent();
            DragContainer container = new DragContainer();

            container.addData("type", type.toString());
            content.put(DragContainer.ADD_NODE, container);

            startDragAndDrop(TransferMode.ANY).setContent(content);

            event.consume();
        });
    }

    private void buildLinkDragHandlers() {

        linkHandleDragDetected = (MouseEvent event) -> {
            getParent().setOnDragOver(null);
            getParent().setOnDragDropped(null);

            getParent().setOnDragOver(contextLinkDragOver);
            getParent().setOnDragDropped(contextLinkDragDropped);

            //Set up user-draggable link
            rightPane.getChildren().add(0, dragLink);

            dragLink.setVisible(false);

            Point2D p = new Point2D(
                    getLayoutX() + (getWidth() / 2.0),
                    getLayoutY() + (getHeight() / 2.0)
            );

            dragLink.setStart(p);

            //Drag content code
            ClipboardContent content = new ClipboardContent();
            DragContainer container = new DragContainer();

            //pass the UUID of the source node for later lookup
            container.addData("source", getId());

            content.put(DragContainer.ADD_LINK, container);

            startDragAndDrop(TransferMode.ANY).setContent(content);

            event.consume();
        };

        linkHandleDragDropped = (DragEvent event) -> {
            getParent().setOnDragOver(null);
            getParent().setOnDragDropped(null);

            //get the drag data.  If it's null, abort.
            //This isn't the drag event we're looking for.
            DragContainer container
                    = (DragContainer) event.getDragboard().getContent(DragContainer.ADD_LINK);

            if (container == null) {
                return;
            }

            //hide the draggable NodeLink and remove it from the right-hand AnchorPane's children
            dragLink.setVisible(false);
            rightPane.getChildren().remove(0);

            AnchorPane linkHandle = (AnchorPane) event.getSource();

            ClipboardContent content = new ClipboardContent();

            //pass the UUID of the target node for later lookup
            container.addData("target", getId());

            content.put(DragContainer.ADD_LINK, container);

            event.getDragboard().setContent(content);
            event.setDropCompleted(true);
            event.consume();
        };

        contextLinkDragOver = (DragEvent event) -> {
            event.acceptTransferModes(TransferMode.ANY);

            //Relocate end of user-draggable link
            if (!dragLink.isVisible()) {
                dragLink.setVisible(true);
            }

            dragLink.setEnd(new Point2D(event.getX(), event.getY()));

            event.consume();
        };

        //drop event for link creation
        contextLinkDragDropped = (DragEvent event) -> {
            System.out.println("context link drag dropped");

            getParent().setOnDragOver(null);
            getParent().setOnDragDropped(null);

            //hide the draggable NodeLink and remove it from the right-hand AnchorPane's children
            dragLink.setVisible(false);
            rightPane.getChildren().remove(0);

            event.setDropCompleted(true);
            event.consume();
        };

    }

    @Override
    public String toString() {
        return getLabel();
    }
}
