/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.model.BestEffortPathCollection;
import com.mpls.model.LinksCollection;
import com.mpls.model.NodePathModel;
import com.mpls.model.NodesCollection;
import java.awt.Color;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Mensi Kais
 */
public class Paths extends AnchorPane implements Initializable {

    private FXMLLoader loader;
    private NodesCollection nodes;

    @FXML
    private ListView<NodePathModel> beginList;
    @FXML
    private ListView<NodePathModel> finalList;

    public Paths(NodesCollection nodes) {
        this.nodes = nodes;
        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/paths.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        beginList.getSelectionModel().setSelectionModel(SelectionMode.SINGLE);

        nodes.getNodesCollection().forEach(node -> {
            NodePathModel npm = new NodePathModel();
            npm.setUid(node.getId());
            npm.setLabel(node.getLabel());
            beginList.getItems().add(npm);
        });
    }

    public BestEffortPathCollection createPath(LinksCollection links, String linkType) {
        Color color = new Color(new Random().nextInt(0xFFFFFF));
        String hex = String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());
        BestEffortPathCollection beCollection = new BestEffortPathCollection();
        List<DraggableNode> nodesCollection = new LinkedList<>();
        List<NodeLink> linksCollection = new LinkedList<>();

        finalList.getItems().forEach(nodeFinal -> {
            nodes.getNodesCollection().forEach(node -> {

                if (node.getId().equals(nodeFinal.getUid())) {
                    nodesCollection.add(node);
                }
            });
        });

        links.getLinksCollection().forEach(link -> {
            if (!nodesCollection.isEmpty()) {
                DraggableNode node0 = nodesCollection.get(0);
                for (int i = 1; i < nodesCollection.size(); i++) {
                    //the same link id is linking the 2 same nodes
                    if (node0.getLinkIds().contains(link.getId()) && nodesCollection.get(i).getLinkIds().contains(link.getId())) {
                        //link.setType(linkType);
                        link.setStyle("-fx-stroke : " + hex + ";");
                        linksCollection.add(link);
                    }

                    node0 = nodesCollection.get(i);
                }
            }
//            finalCollection.forEach(node1 -> {
//                finalCollection.forEach(node2 -> {
//                    //the same link id is linking 2 nodes
//                    if (!node1.equals(node2) && node1.getLinkIds().contains(link.getId()) && node2.getLinkIds().contains(link.getId())) {
//                        link.setType("RED");
//                    }
//                });
//            });
        });

        beCollection.setPathNodesList(nodesCollection);
        beCollection.setPathLinksList(linksCollection);

        return beCollection;
    }

    @FXML
    private void addToFinalList(ActionEvent event) {
        if (beginList.getSelectionModel().getSelectedItem() != null) {
            finalList.getItems().add(beginList.getSelectionModel().getSelectedItem());
            beginList.getItems().remove(beginList.getSelectionModel().getSelectedItem());
        }
    }

    @FXML
    private void removeToBeginList(ActionEvent event) {
        if (finalList.getSelectionModel().getSelectedItem() != null) {
            beginList.getItems().add(finalList.getSelectionModel().getSelectedItem());
            finalList.getItems().remove(finalList.getSelectionModel().getSelectedItem());
        }
    }

    public ListView<NodePathModel> getFinalList() {
        return finalList;
    }
}
