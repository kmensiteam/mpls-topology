/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import com.mpls.view.node.BEClassConfig;
import com.mpls.view.node.DSClassConfig;
import com.mpls.view.node.SClassConfig;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;

/**
 * FXML Controller class
 *
 * @author kmens
 */
public class NodeConfig extends AnchorPane implements Initializable {

    private FXMLLoader loader;

    @FXML
    private BorderPane borderBane;
    
    private PropertySheetNode propertySheetNode;

    private BEClassConfig beClassConfig;
    private DSClassConfig dsClassConfig;
    private SClassConfig sClassConfig;

    private PopOver popOver;

    private TrafficConfig trafficConfig;

    public NodeConfig(
            PropertySheetNode propertySheetNode,
            BEClassConfig beClassConfig,
            DSClassConfig dsClassConfig,
            SClassConfig sClassConfig,
            PopOver popOver
    ) {
        this.propertySheetNode = propertySheetNode;
        this.beClassConfig = beClassConfig;
        this.dsClassConfig = dsClassConfig;
        this.sClassConfig = sClassConfig;
        this.popOver = popOver;

        try {
            loader = new FXMLLoader(getClass().getResource("/com/mpls/fxml/node-config.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        borderBane.setCenter(propertySheetNode);
    }

    @FXML
    private void addTraffic(ActionEvent event) {
        trafficConfig = new TrafficConfig(
                beClassConfig,
                dsClassConfig,
                sClassConfig
        );

        Dialog dialogElasticTraffic = new Dialog();
        dialogElasticTraffic.initOwner(popOver.getScene().getWindow());
        dialogElasticTraffic.setTitle("Traffic Config");

        Stage stage = (Stage) dialogElasticTraffic.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/com/mpls/images/traffic.png")));

        dialogElasticTraffic.getDialogPane().getButtonTypes().addAll(ButtonType.OK);
        dialogElasticTraffic.getDialogPane().setContent(trafficConfig);
        dialogElasticTraffic.showAndWait();
    }

}
