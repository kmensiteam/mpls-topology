/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.input.DataFormat;
import javafx.util.Pair;

/**
 *
 * @author Mensi Kais
 */
public class DragContainer implements Serializable {

    private static final long serialVersionUID = -1890998765646621338L;

    public static final DataFormat ADD_NODE = new DataFormat("mpls.DragIcon.add");

    public static final DataFormat DRAG_NODE = new DataFormat("mpls.DraggableNode.drag");

    public static final DataFormat ADD_LINK = new DataFormat("mpls.NodeLink.add");

    private final List<Pair<String, Object>> dataPairs = new ArrayList<>();

    public void addData(String key, Object value) {
        dataPairs.add(new Pair<>(key, value));
    }

    public <T> T getValue(String key) {

        return (T) dataPairs
                .stream()
                .filter(data -> data.getKey().equals(key))
                .map(data -> data.getValue())
                .findFirst()
                .orElse(null);
    }

    public List<Pair<String, Object>> getData() {
        return dataPairs;
    }
}
