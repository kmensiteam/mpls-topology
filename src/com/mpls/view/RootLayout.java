/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.view;

import java.io.IOException;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Mensi Kais
 */
public class RootLayout extends AnchorPane {

//    private final NodesCollection nodes;
//    private final LinksCollection links;
    private FXMLLoader loder;

    @FXML
    private SplitPane basePane;
    @FXML
    private AnchorPane rightPane;
    @FXML
    private VBox leftPane;

    private DragIcon dragOverIcon = null;

    private EventHandler iconDragOverRoot = null;
    private EventHandler iconDragDropped = null;
    private EventHandler iconDragOverRightPane = null;

    public RootLayout() {
        try {
            loder = new FXMLLoader(getClass().getResource("/com/mpls/fxml/root-layout.fxml"));
            loder.setRoot(this);
            loder.setController(this);
            loder.load();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @FXML
    public void initialize() {

        dragOverIcon = new DragIcon();
        dragOverIcon.setVisible(false);
        dragOverIcon.setOpacity(0.65);

        getChildren().add(dragOverIcon);

//        for (int i = 0; i < 7; i++) {
//            DragIcon icon = new DragIcon();
//            icon.setType(DragIconType.values()[i]);
//            addDragDetection(icon);
//            leftPane.getChildren().add(icon);
//        }

        DragIcon iconLER = new DragIcon();
        iconLER.setType(DragIconType.LER);
        addDragDetection(iconLER);
        leftPane.getChildren().add(iconLER);
        
        DragIcon iconLSR = new DragIcon();
        iconLSR.setType(DragIconType.LSR);
        addDragDetection(iconLSR);
        leftPane.getChildren().add(iconLSR);

        buildDragHandlers();
    }

    public AnchorPane getRightPane() {
        return rightPane;
    }

    private void addDragDetection(DragIcon dragIcon) {

        dragIcon.setOnDragDetected((MouseEvent event) -> {
            // set drag event handlers on their respective objects
            basePane.setOnDragOver(iconDragOverRoot);
            rightPane.setOnDragOver(iconDragOverRightPane);
            rightPane.setOnDragDropped(iconDragDropped);

            // get a reference to the clicked DragIcon object
            DragIcon icn = (DragIcon) event.getSource();

            //begin drag ops
            dragOverIcon.setType(icn.getType());
            dragOverIcon.relocateToPoint(new Point2D(event.getSceneX(), event.getSceneY()));

            ClipboardContent content = new ClipboardContent();
            DragContainer container = new DragContainer();

            container.addData("type", dragOverIcon.getType().toString());
            content.put(DragContainer.ADD_NODE, container);

            dragOverIcon.startDragAndDrop(TransferMode.ANY).setContent(content);
            dragOverIcon.setVisible(true);
            dragOverIcon.setMouseTransparent(true);
            event.consume();
        });
    }

    private void buildDragHandlers() {

        //drag over transition to move widget form left pane to right pane
        iconDragOverRoot = (EventHandler<DragEvent>) (DragEvent event) -> {
            Point2D p = rightPane.sceneToLocal(event.getSceneX(), event.getSceneY());

            //turn on transfer mode and track in the right-pane's context
            //if (and only if) the mouse cursor falls within the right pane's bounds.
            if (!rightPane.boundsInLocalProperty().get().contains(p)) {

                event.acceptTransferModes(TransferMode.ANY);
                dragOverIcon.relocateToPoint(new Point2D(event.getSceneX(), event.getSceneY()));
                return;
            }

            event.consume();
        };

        iconDragOverRightPane = (EventHandler<DragEvent>) (DragEvent event) -> {
            event.acceptTransferModes(TransferMode.ANY);

            //convert the mouse coordinates to scene coordinates,
            //then convert back to coordinates that are relative to
            //the parent of mDragIcon.  Since mDragIcon is a child of the root
            //pane, coodinates must be in the root pane's coordinate system to work
            //properly.
            dragOverIcon.relocateToPoint(
                    new Point2D(event.getSceneX(), event.getSceneY())
            );
            event.consume();
        };

        iconDragDropped = (EventHandler<DragEvent>) (DragEvent event) -> {
            DragContainer container
                    = (DragContainer) event.getDragboard().getContent(DragContainer.ADD_NODE);

            container.addData("scene_coords",
                    new Point2D(event.getSceneX(), event.getSceneY()));

            ClipboardContent content = new ClipboardContent();
            content.put(DragContainer.ADD_NODE, container);

            event.getDragboard().setContent(content);
            event.setDropCompleted(true);
        };

        this.setOnDragDone((DragEvent event) -> {
            rightPane.removeEventHandler(DragEvent.DRAG_OVER, iconDragOverRightPane);
            rightPane.removeEventHandler(DragEvent.DRAG_DROPPED, iconDragDropped);
            basePane.removeEventHandler(DragEvent.DRAG_OVER, iconDragOverRoot);

            dragOverIcon.setVisible(false);

            //Create node drag operation
            DragContainer container
                    = (DragContainer) event.getDragboard().getContent(DragContainer.ADD_NODE);

            if (container != null) {
                if (container.getValue("scene_coords") != null) {

                    DraggableNode node = new DraggableNode();
                    node.setType(DragIconType.valueOf(container.getValue("type")));

                    rightPane.getChildren().add(node);
                    Point2D cursorPoint = container.getValue("scene_coords");
                    node.relocateToPoint(
                            new Point2D(cursorPoint.getX() - 32, cursorPoint.getY() - 32)
                    );
                }
            }
            /*
            //Move node drag operation
            container =
            (DragContainer) event.getDragboard().getContent(DragContainer.DragNode);
            
            if (container != null) {
            if (container.getValue("type") != null)
            System.out.println ("Moved node " + container.getValue("type"));
            }
             */

            //AddLink drag operation
            container
                    = (DragContainer) event.getDragboard().getContent(DragContainer.ADD_LINK);

            if (container != null) {

                //bind the ends of our link to the nodes whose id's are stored in the drag container
                String sourceId = container.getValue("source");
                String targetId = container.getValue("target");

                if (sourceId != null && targetId != null) {

                    //	System.out.println(container.getData());
                    NodeLink link = new NodeLink();

                    link.getModel().setSourceId(sourceId);
                    link.getModel().setTargetId(targetId);

                    //add our link at the top of the rendering order so it's rendered first
                    rightPane.getChildren().add(0, link);

                    DraggableNode source = null;
                    DraggableNode target = null;

                    for (Node n : rightPane.getChildren()) {

                        if (n.getId() == null) {
                            continue;
                        }

                        if (n.getId().equals(sourceId)) {
                            source = (DraggableNode) n;
                        }

                        if (n.getId().equals(targetId)) {
                            target = (DraggableNode) n;
                        }

                    }

                    if (source != null && target != null) {
                        link.bindEnds(source, target);
                    }
                }

            }

            event.consume();
        });
    }

}
