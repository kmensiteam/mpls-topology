/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls;

import com.gluonhq.ignite.spring.SpringContext;
import com.mpls.security.view.LoginDialog;
import java.util.Arrays;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.inject.Inject;

/**
 *
 * @author Mensi Kais
 */
public class MPLSApplication extends Application {

    private final SpringContext context = new SpringContext(this, () -> Arrays.asList(MPLSApplication.class.getPackage().getName()));

    @Inject
    FXMLLoader fxmlLoader;
    
    @Inject
    LoginDialog loginDialog;

    @Override
    public void start(Stage stage) throws Exception {
        context.init();

        fxmlLoader.setLocation(getClass().getResource("fxml/view.fxml"));
        Parent view = fxmlLoader.load();

        stage.setTitle("MPLS Application");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("images/graph.jpg")));

        Scene scene = new Scene(view, 800, 600);
        scene.getStylesheets().add(getClass().getResource("styles/style.css").toExternalForm());

        stage.setScene(scene);
        stage.setMaximized(true);
//        stage.setResizable(false);
        stage.show();

        //TODO login
        loginDialog.init(stage);
        
        stage.setOnCloseRequest(e -> {
            System.out.println("closing.........");
            Platform.exit();
            System.exit(0);
        });
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
