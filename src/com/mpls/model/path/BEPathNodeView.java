/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.path;

import com.mpls.view.DraggableNode;

/**
 *
 * @author Mensi Kais
 */
@lombok.Data
public class BEPathNodeView {

    private String nodeLabel;
    private DraggableNode node;

    @Override
    public String toString() {
        return nodeLabel;
    }

}
