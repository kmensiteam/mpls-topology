/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mensi Kais
 */
@lombok.Data
public class LinkModel {

    private String uid;
    private String type;
    private String sourceId;
    private String targetId;
    private double cBE;
    private double cDS;
    private double cH;
    private int nvpDS;
    private List<Double> vpdsWFQ = new ArrayList<>();
    private int nvpH;
    private List<Double> vphWFQ = new ArrayList<>();
}
