/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model;

import com.mpls.view.DraggableNode;
import com.mpls.view.NodeLink;
import java.util.List;

/**
 *
 * @author Mensi Kais
 */
@lombok.Data
public class BestEffortPathCollection {

    private List<DraggableNode> pathNodesList;
    private List<NodeLink> pathLinksList;
}
