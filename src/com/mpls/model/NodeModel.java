/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model;

import com.mpls.domain.Point;
import com.mpls.model.node.BEClass;
import com.mpls.model.node.DSClass;
import com.mpls.model.node.SClass;
import java.util.List;

/**
 *
 * @author Mensi Kais
 */
@lombok.Data
public class NodeModel {

    private String uid;
    private String type;
    private Point point;
    private String id;
    private String name;
    private String ip;
    private List<BEClass> beClasses;
    private List<DSClass> dsClasses;
    private List<SClass> sClasses;
}
