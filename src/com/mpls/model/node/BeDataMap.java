/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.node;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Mensi Kais
 */
public class BeDataMap {

    private final Map<String, Object> nodesMap = new LinkedHashMap<>();

    public BeDataMap(BEClass beClass) {
        nodesMap.put("Elastic Config#di(Mbits/s)", beClass.getD() == 0d ? 30d : beClass.getD());
        nodesMap.put("Elastic Config#ʎi(flows/s)", beClass.getLambda() == 0d ? 6d : beClass.getLambda());
        nodesMap.put("Elastic Config#σi(Mbits/s)", beClass.getSigma() == 0d ? 12d : beClass.getSigma());
    }

    public Object get(String key) {
        return nodesMap.get(key);
    }

    public void put(String key, Object value) {
        nodesMap.put(key, value);
    }

    public Map<String, Object> getNodesMap() {
        return nodesMap;
    }
}
