/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.node;

/**
 *
 * @author Mensi Kais
 */
@lombok.Data
public class SClass {

    private String label;

    private double d;
    private double lambda;
    private double tau;

    public SClass() {
    }

    public SClass(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }
}
