/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.node;

import com.mpls.model.NodeModel;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Mensi Kais
 */
public class NodesDataMap {

    private final Map<String, Object> nodesMap = new LinkedHashMap<>();
    
    private NodeModel nodeModel;

    public NodesDataMap(NodeModel nodeModel) {
        this.nodeModel = nodeModel;
        nodesMap.put("Router Config#Router Id", nodeModel.getId() == null ? "01" : nodeModel.getId());
        nodesMap.put("Router Config#Router Name", nodeModel.getName() == null ? "router" : nodeModel.getName());
        nodesMap.put("Router Config#Router IP", nodeModel.getIp() == null ? "192.168.1.5" : nodeModel.getIp());
    }

    public Object get(String key) {
        return nodesMap.get(key);
    }

    public void put(String key, Object value) {
        nodesMap.put(key, value);
    }

    public Map<String, Object> getNodesMap() {
        return nodesMap;
    }

    public NodeModel getNodeModel() {
        return nodeModel;
    }    
}
