/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.node;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Mensi Kais
 */
public class StreamingDataMap {

    private final Map<String, Object> nodesMap = new LinkedHashMap<>();

    public StreamingDataMap(SClass sClass) {
        nodesMap.put("Streaming Config#dj(Mbits/s)", sClass.getD() == 0d ? 30d : sClass.getD());
        nodesMap.put("Streaming Config#ʎj(flows/s)", sClass.getLambda() == 0d ? 600d : sClass.getLambda());
        nodesMap.put("Streaming Config#τi(s)", sClass.getTau() == 0d ? 2d : sClass.getTau());
    }

    public Object get(String key) {
        return nodesMap.get(key);
    }

    public void put(String key, Object value) {
        nodesMap.put(key, value);
    }

    public Map<String, Object> getNodesMap() {
        return nodesMap;
    }
}
