/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.node;

import java.util.Optional;
import javafx.beans.value.ObservableValue;
import org.controlsfx.control.PropertySheet;

/**
 *
 * @author Mensi Kais
 */
public class DivPropertyItem implements PropertySheet.Item {

    private DivDataMap divData;

    private String key;
    private String category;
    private String name;

    public DivPropertyItem() {
    }

    public void setElasticData(DivDataMap divData) {
        this.divData = divData;
    }

    public void setKey(String key) {
        this.key = key;
        String[] skey = key.split("#");
        category = skey[0];
        name = skey[1];
    }

    @Override
    public Class<?> getType() {
        return divData.get(key).getClass();
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Object getValue() {
        return divData.get(key);
    }

    @Override
    public void setValue(Object value) {
        divData.put(key, value);
    }

    @Override
    public Optional<ObservableValue<? extends Object>> getObservableValue() {
        return Optional.empty();
    }

}
