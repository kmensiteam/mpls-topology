/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.node;

/**
 *
 * @author Mensi Kais
 */
@lombok.Data
public class BEClass {
    
    private String label;
    private String linkType;
    
    private double d;
    private double lambda;
    private double sigma;
    
    public BEClass() {
    }
    
    public BEClass(String label) {
        this.label = label;
    }
    
    @Override
    public String toString() {
        return label;
    }
}
