/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.node;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Mensi Kais
 */
public class DivDataMap {

    private final Map<String, Object> nodesMap = new LinkedHashMap<>();

    public DivDataMap(DSClass dsClass) {
        nodesMap.put("Elastic Config#di(Mbits/s)", dsClass.getD() == 0d ? 30d : dsClass.getD());
        nodesMap.put("Elastic Config#ʎi(flows/s)", dsClass.getLambda() == 0d ? 600d : dsClass.getLambda());
        nodesMap.put("Elastic Config#σi(Mbits/s)", dsClass.getSigma() == 0d ? 12d : dsClass.getSigma());
    }

    public Object get(String key) {
        return nodesMap.get(key);
    }

    public void put(String key, Object value) {
        nodesMap.put(key, value);
    }

    public Map<String, Object> getNodesMap() {
        return nodesMap;
    }
}
