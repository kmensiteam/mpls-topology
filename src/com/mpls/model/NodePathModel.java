/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model;

/**
 *
 * @author Mensi kais
 */
@lombok.Data
public class NodePathModel {

    private String uid;
    private String label;

    @Override
    public String toString() {
        return getLabel();
    }
}
