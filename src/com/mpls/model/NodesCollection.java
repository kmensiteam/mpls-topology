/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model;

import com.mpls.view.DraggableNode;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Mensi Kais
 */
public class NodesCollection {

    private final List<DraggableNode> nodesCollection = new LinkedList<>();

    public void addNode(DraggableNode node) {
        nodesCollection.add(node);
    }

    public DraggableNode removeNode(DraggableNode node) {
        return nodesCollection.remove(nodesCollection.indexOf(node));
    }

    public List<DraggableNode> getNodesCollection() {
        return nodesCollection;
    }

}
