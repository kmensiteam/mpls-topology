/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.link;

import com.mpls.model.LinkModel;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Mensi Kais
 */
public class LinkHDataMap {

    private final Map<String, Object> nodesMap = new LinkedHashMap<>();

    public LinkHDataMap(LinkModel linkModel) {
        nodesMap.put("Heterogeneous Config#C(Mbits/s)", linkModel.getCH() == 0d ? 300d : linkModel.getCH());
        nodesMap.put("Heterogeneous Config#WFQ Number", linkModel.getNvpH() == 0 ? 3 : linkModel.getNvpH());
    }

    public Object get(String key) {
        return nodesMap.get(key);
    }

    public void put(String key, Object value) {
        nodesMap.put(key, value);
    }

    public Map<String, Object> getNodesMap() {
        return nodesMap;
    }
}
