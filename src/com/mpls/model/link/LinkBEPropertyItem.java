/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.link;

import com.mpls.model.*;
import java.util.Optional;
import javafx.beans.value.ObservableValue;
import org.controlsfx.control.PropertySheet;

/**
 *
 * @author Mensi Kais
 */
public class LinkBEPropertyItem implements PropertySheet.Item {

    private LinkBEDataMap linkBEDataMap;

    private String key;
    private String category;
    private String name;

    public LinkBEPropertyItem() {
    }

    public void setElasticData(LinkBEDataMap linkBEDataMap) {
        this.linkBEDataMap = linkBEDataMap;
    }

    public void setKey(String key) {
        this.key = key;
        String[] skey = key.split("#");
        category = skey[0];
        name = skey[1];
    }

    @Override
    public Class<?> getType() {
        return linkBEDataMap.get(key).getClass();
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Object getValue() {
        return linkBEDataMap.get(key);
    }

    @Override
    public void setValue(Object value) {
        linkBEDataMap.put(key, value);
    }

    @Override
    public Optional<ObservableValue<? extends Object>> getObservableValue() {
        return Optional.empty();
    }

}
