/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.link;

import com.mpls.model.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Mensi Kais
 */
public class LinkBEDataMap {

    private final Map<String, Object> nodesMap = new LinkedHashMap<>();

    public LinkBEDataMap(LinkModel linkModel) {
        nodesMap.put("Elastic Config#C(Mbits/s)", linkModel.getCBE() == 0d ? 300d : linkModel.getCBE());
    }

    public Object get(String key) {
        return nodesMap.get(key);
    }

    public void put(String key, Object value) {
        nodesMap.put(key, value);
    }

    public Map<String, Object> getNodesMap() {
        return nodesMap;
    }
}
