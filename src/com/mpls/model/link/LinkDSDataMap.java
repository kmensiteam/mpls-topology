/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.link;

import com.mpls.model.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Mensi Kais
 */
public class LinkDSDataMap {

    private final Map<String, Object> nodesMap = new LinkedHashMap<>();

    public LinkDSDataMap(LinkModel linkModel) {
        nodesMap.put("Elastic Config#C(Mbits/s)", linkModel.getCDS() == 0d ? 300d : linkModel.getCDS());
        nodesMap.put("Elastic Config#WFQ Number", linkModel.getNvpDS() == 0 ? 3 : linkModel.getNvpDS());        
    }

    public Object get(String key) {
        return nodesMap.get(key);
    }

    public void put(String key, Object value) {
        nodesMap.put(key, value);
    }

    public Map<String, Object> getNodesMap() {
        return nodesMap;
    }
}
