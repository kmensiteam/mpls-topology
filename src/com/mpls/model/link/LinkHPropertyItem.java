/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model.link;

import java.util.Optional;
import javafx.beans.value.ObservableValue;
import org.controlsfx.control.PropertySheet;

/**
 *
 * @author Mensi Kais
 */
public class LinkHPropertyItem implements PropertySheet.Item {

    private LinkHDataMap linkHDataMap;

    private String key;
    private String category;
    private String name;

    public LinkHPropertyItem() {
    }

    public void setElasticData(LinkHDataMap linkHDataMap) {
        this.linkHDataMap = linkHDataMap;
    }

    public void setKey(String key) {
        this.key = key;
        String[] skey = key.split("#");
        category = skey[0];
        name = skey[1];
    }

    @Override
    public Class<?> getType() {
        return linkHDataMap.get(key).getClass();
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Object getValue() {
        return linkHDataMap.get(key);
    }

    @Override
    public void setValue(Object value) {
        linkHDataMap.put(key, value);
    }

    @Override
    public Optional<ObservableValue<? extends Object>> getObservableValue() {
        return Optional.empty();
    }

}
