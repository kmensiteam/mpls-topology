/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.model;

import com.mpls.view.NodeLink;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Mensi kais
 */
public class LinksCollection {

    private final List<NodeLink> linksCollection = new LinkedList<>();

    public void addLink(NodeLink link) {
        linksCollection.add(link);
    }

    public NodeLink removeNode(NodeLink link) {
        return linksCollection.remove(linksCollection.indexOf(link));
    }

    public List<NodeLink> getLinksCollection() {
        return linksCollection;
    }
}
