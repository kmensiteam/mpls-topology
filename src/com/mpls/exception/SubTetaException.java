/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.exception;

/**
 *
 * @author Mensi Kais
 */
public class SubTetaException extends RuntimeException {

    public SubTetaException(String message) {
        super(message);
    }

    public SubTetaException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public SubTetaException(Throwable throwable) {
        super(throwable);
    }

}
