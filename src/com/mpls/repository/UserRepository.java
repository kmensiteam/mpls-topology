/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.repository;

import com.mpls.domain.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Mensi Kais
 */
public interface UserRepository extends MongoRepository<User, ObjectId> {

    public User findByLogin(@Param("login") String login);
}
