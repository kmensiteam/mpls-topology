/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.report;

import com.itextpdf.io.font.FontConstants;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import java.io.IOException;

/**
 *
 * @author Mensi Kais
 */
public class PdfReport {

    private StringBuilder sb;

    public byte[] createBestEffortReport(BestEffortReportModel bestEffortReportModel) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        //Initialize PDF writer
        PdfWriter writer = new PdfWriter(baos);

        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(writer);

        // Initialize document
        Document document = new Document(pdf);

        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);

        //Add paragraph to the document
        Paragraph p = new Paragraph("Best Effort Simulation Report!")
                .setTextAlignment(TextAlignment.CENTER).setFont(font).setFontSize(14);
        document.add(p);

        bestEffortReportModel.getBeClassesMap().forEach((k, v) -> {
            document.add(new Paragraph(k.getLabel() + ":").setFont(font).setFontSize(12));
            document.add(new Paragraph("nodes: ").setFont(font).setFontSize(12));
            sb = new StringBuilder();
            v.getPathNodesList().forEach(node -> {
                sb.append(" -> ")
                        .append(node.getLabel())
                        .append(" (").append("d(Mbits/s): ").append(k.getD()).append(" - ʎ(flows/s): ").append(k.getLambda()).append(" - σ(Mbits/s): ").append(k.getSigma()).append(")");
                document.add(new Paragraph(sb.toString()).setFont(font).setFontSize(12));
                sb = new StringBuilder();
            });

            document.add(new Paragraph("links: ").setFont(font).setFontSize(12));
            v.getPathLinksList().forEach(link -> {
                sb.append(" -> ")
                        .append(link.getModel().getUid())
                        .append(" (").append("C(Mbits/s): ").append(link.getModel().getCBE()).append(")");
                document.add(new Paragraph(sb.toString()).setFont(font).setFontSize(12));
                sb = new StringBuilder();
            });

        });

        document.add(new Paragraph());

        bestEffortReportModel.getBestEffortDebitMoyen().forEach((k, v) -> {
            document.add(new Paragraph(k.getLabel() + "  debit moyen: " + v).setFont(font).setFontSize(12));
        });

        //Close document
        document.close();

        return baos.toByteArray();
    }
}
