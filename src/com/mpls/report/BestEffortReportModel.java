/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mpls.report;

import com.mpls.domain.Point;
import com.mpls.model.BestEffortPathCollection;
import com.mpls.model.LinkModel;
import com.mpls.model.LinksCollection;
import com.mpls.model.NodeModel;
import com.mpls.model.NodesCollection;
import com.mpls.model.node.BEClass;
import com.mpls.view.DraggableNode;
import com.mpls.view.NodeLink;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Mensi Kais
 */
@lombok.Data
public class BestEffortReportModel {

    private NodesCollection nodes = new NodesCollection();
    private LinksCollection links = new LinksCollection();

    private List<NodeModel> nodesModel = new LinkedList<>();
    private List<LinkModel> linksModel = new LinkedList<>();
    
    private Map<BEClass, BestEffortPathCollection> beClassesMap;

    private Map<BEClass, Double> bestEffortDebitMoyen;

    public void addModel(AnchorPane rightPane) {
        nodes.getNodesCollection().clear();
        links.getLinksCollection().clear();

        rightPane.getChildren().forEach(node -> {
            if (node instanceof DraggableNode) {
                String label = (String) ((DraggableNode) node).getPropertySheetNode()
                        .getItemsList()
                        .stream()
                        .filter(item -> item.getName().equals("Router Id"))
                        .map(item -> item.getValue())
                        .findFirst()
                        .get();
                ((DraggableNode) node).setLabel(label);
                nodes.addNode((DraggableNode) node);
            } else if (node instanceof NodeLink) {
                links.addLink((NodeLink) node);
            }
        });

        nodes.getNodesCollection().forEach(node -> {

            node.getModel().setType(
                    node.getType().name()
            );

            //get node layotX rt layoutY
            node.getModel().setPoint(new Point(node.getLayoutX(), node.getLayoutY()));

            node.getModel().setId(
                    (String) node.getPropertySheetNode()
                            .getItemsList()
                            .stream()
                            .filter(item -> item.getName().equals("Router Id"))
                            .map(item -> item.getValue())
                            .findFirst()
                            .get()
            );
            node.getModel().setName(
                    (String) node.getPropertySheetNode()
                            .getItemsList()
                            .stream()
                            .filter(item -> item.getName().equals("Router Name"))
                            .map(item -> item.getValue())
                            .findFirst()
                            .get()
            );
            node.getModel().setIp(
                    (String) node.getPropertySheetNode()
                            .getItemsList()
                            .stream()
                            .filter(item -> item.getName().equals("Router IP"))
                            .map(item -> item.getValue())
                            .findFirst()
                            .get()
            );
            node.getModel().getBeClasses().clear();
            node.getModel().setBeClasses(
                    node.getBeClassConfig().getBeClassList()
            );
            node.getModel().getDsClasses().clear();
            node.getModel().setDsClasses(
                    node.getDsClassConfig().getDsClassList()
            );
            node.getModel().getSClasses().clear();
            node.getModel().setSClasses(
                    node.getSClassConfig().getSClassList()
            );
            nodesModel.add(node.getModel());
        });

        links.getLinksCollection().forEach(link -> {
            link.getModel().setType(link.getType());

            link.getModel().setCBE(
                    (double) link.getPropertySheetBestEffortLink()
                            .getItemsList()
                            .stream()
                            .filter(item -> item.getName().equals("C(Mbits/s)"))
                            .map(item -> item.getValue())
                            .findFirst()
                            .get()
            );

            link.getModel().setCDS(
                    (double) link.getPropertySheetDivServiceLink()
                            .getItemsList()
                            .stream()
                            .filter(item -> item.getName().equals("C(Mbits/s)"))
                            .map(item -> item.getValue())
                            .findFirst()
                            .get()
            );

            link.getModel().setNvpDS(
                    (int) link.getPropertySheetDivServiceLink()
                            .getItemsList()
                            .stream()
                            .filter(item -> item.getName().equals("WFQ Number"))
                            .map(item -> item.getValue())
                            .findFirst()
                            .get()
            );

            link.getModel().setVpdsWFQ(
                    link.getvPSDConfig().getVpdsDoubleList()
            );

            link.getModel().setCH(
                    (double) link.getPropertySheetHeterogeneousLink()
                            .getItemsList()
                            .stream()
                            .filter(item -> item.getName().equals("C(Mbits/s)"))
                            .map(item -> item.getValue())
                            .findFirst()
                            .get()
            );

            link.getModel().setNvpH(
                    (int) link.getPropertySheetHeterogeneousLink()
                            .getItemsList()
                            .stream()
                            .filter(item -> item.getName().equals("WFQ Number"))
                            .map(item -> item.getValue())
                            .findFirst()
                            .get()
            );

            link.getModel().setVphWFQ(
                    link.getvPHConfig().getVphDoubleList()
            );

            linksModel.add(link.getModel());
        });
    }
}
